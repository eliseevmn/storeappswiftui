//
//  AuthKeys.swift
//  SarawanSwiftUI
//
//  Created by MAC on 18.01.2022.
//

import Foundation

struct AuthKeys {
    static let isLoggedIn = "isLoggedIn"
    static let hasAccount = "hasAccount"
    static let phoneNumber = "phoneNumber"
    static let salt = "salt"
    static let token = "token"
}
