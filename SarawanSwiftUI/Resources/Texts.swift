//
//  Texts.swift
//  Sarawan
//
//  Created by MAC on 30.11.2021.
//

import Foundation

enum Texts {

    enum Main {
        static let ourStores = "Наши магазины"
        static let favorable = "Выгодные предложения"
        static let reccomend = "Мы рекомендуем"
        static let minPrice = "Мин"
    }

    enum FoodCard {
        static let minPrice = "Мин. цена"
    }

    enum AboutProject {
        static let about = "О нас"
        static let info = "Мы хотим, чтобы у вас оставалось больше времени на то, что вы любите. С нашим сервисом вам не придется отвлекаться на походы по магазинам, вы сможете сосредоточиться на своей семье и любимом деле. А все нужные продукты вам доставит Сарафан."
        static let benefits = "Наши преимущества"
        static let wideAssortment  = "Широкий ассортимент"
        static let wideAssortmentDescription  = "На сайте представлены все крупные торговые сети"
        static let freshFood = "Только свежие продукты"
        static let freshFoodDescription  = "Обязательная проверка качества перед доставкой"
        static let costSaving = "Экономия средств"
        static let costSavingDescription = "Система сравнивает цены в разных магазинах и выбирает товар с самой низкой ценой по городу"
        static let fastShipping = "Быстрая доставка"
        static let fastShippingDescription = "Ваши любимые продукты из разных магазинов города будут на вашем столе уже завтра"
    }

    enum HowWeWorkInfo {
        static let chooseProduct = "Выберите продукты в каталоге. Система сама подберет вам самые выгодные предложения."
        static let delivery = "Оформите доставку на ближайшее время или нужную дату. Привезем ваш заказ в указанный срок."
        static let subscription = "Оформите подписку и экономьте время, создавая готовые корзины для любых случаев."
    }

    enum SupportProject {
        static let telephoneNumber = "+7 123 456 7890"
    }

    enum CommonInfo {
        static let info = "Инфо"
        static let supportProject = "Служба поддержки"
        static let aboutProject = "О проекте"
        static let howWeWork = "Как мы работаем"
    }

    enum NavigationView {
        static let supportProject = "Служба поддержки"
        static let aboutProject = "О проекте"
        static let howWeWork = "Как мы работаем"
    }

}
