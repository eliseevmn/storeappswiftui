//
//  MovieAppReduxApp.swift
//  Shared
//
//  Created by MAC on 05.01.2022.
//

import SwiftUI

@main
struct MovieAppReduxApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
