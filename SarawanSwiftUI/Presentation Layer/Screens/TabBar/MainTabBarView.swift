//
//  MainTabBarView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 09.01.2022.
//

import SwiftUI

struct MainTabBarView: View {

    // MARK: - Properties
    @EnvironmentObject var tabBarModel: MainTabBarViewModel

    // MARK: - Body
    var body: some View {

        HStack(alignment: .bottom) {
            // Tab № 1
            MainTabItemView(iconImage: "icTabMain", title: "Главная") {
                self.tabBarModel.selection = 0
            }
            .foregroundColor(tabBarModel.selection == 0 ? Color.orangeApp : Color.blackApp)
            .padding(.leading, 20)
            Spacer()

            // Tab № 2
            MainTabItemView(iconImage: "icTabCatalog", title: "Каталог") {
                self.tabBarModel.selection = 1
            }
            .foregroundColor(tabBarModel.selection == 1 ? Color.orangeApp : Color.blackApp)
            Spacer()

            // Tab № 3
            MainTabItemView(iconImage: "icTabBasket", title: "Корзина") {
                self.tabBarModel.selection = 2
            }
            .foregroundColor(tabBarModel.selection == 2 ? Color.orangeApp : Color.blackApp)
            Spacer()

            // Tab № 4
            MainTabItemView(iconImage: "icTabInfo", title: "Инфо") {
                self.tabBarModel.selection = 3
            }
            .foregroundColor(tabBarModel.selection == 3 ? Color.orangeApp : Color.blackApp)
            Spacer()

            // Tab № 5
            MainTabItemView(iconImage: "icTabProfile", title: "Профиль") {
                self.tabBarModel.selection = 4
            }
            .foregroundColor(tabBarModel.selection == 4 ? Color.orangeApp : Color.blackApp)
            .padding(.trailing, 20)
        } // HSTack
        .padding(.top, 2)
        .background(Color.greenBackground)
        .frame(height: 85)
    } // Bode
}

// MARK: - PreviewProvider
struct MainTabBarView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabBarView()
    }
}
