//
//  TabItemView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 09.01.2022.
//

import SwiftUI

struct MainTabItemView: View {

    // MARK: - Properties
    let iconImage: String
    let title: String
    var changeTabItem: (() -> Void)

    // MARK: - Body
    var body: some View {
        VStack {
            Button(action: changeTabItem,
                   label: {
                VStack(spacing: 0) {
                    Image(iconImage)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 25, height: 25)
                    Text(title)
                        .font(.montserratFont(.regular, size: 14))
                    Spacer()
                } // VStack
            }) // Button
            .padding(.top, 7)

            Spacer()
        } // VStack
    } // Bdy
}
