//
//  MainTabBarViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 09.01.2022.
//

import SwiftUI

final class MainTabBarViewModel: ObservableObject {

    // MARK: - Properties
    @Published var selection = 0
}
