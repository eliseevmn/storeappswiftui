//
//  MainTabBar.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import SwiftUI

struct MainTabBar: View {

    // MARK: - Properties
    @EnvironmentObject private var tabBarModel: MainTabBarViewModel

    // MARK: - Body
    var body: some View {
        ZStack {
            if self.tabBarModel.selection == 0 {
                MainView()
            } else if self.tabBarModel.selection == 1 {
                CatalogView()
            } else if self.tabBarModel.selection == 2 {
                EmptyView()
            } else if self.tabBarModel.selection == 3 {
                InfoView()
            } else {
                StartProfileView()
            }

            VStack {
                Spacer()
                MainTabBarView()
            }
            .edgesIgnoringSafeArea(.bottom)
        } // ZStack
    } // Body
}
