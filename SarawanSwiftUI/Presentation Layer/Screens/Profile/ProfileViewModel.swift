//
//  ProfileViewMOdel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 13.01.2022.
//

import Foundation

final class ProfileViewModel: ObservableObject {

    @Published var userModel: UserModel?
    @Published var firstName: String = ""
    @Published var secondName: String = ""
//    @Published var isLogin: Bool = false

    func fullNameUser() -> String {
        guard let userModel = userModel else {
            return "Добавить имя"
        }
        let fullName = userModel.firstName + " " + userModel.lastName
        return fullName
    }
}
