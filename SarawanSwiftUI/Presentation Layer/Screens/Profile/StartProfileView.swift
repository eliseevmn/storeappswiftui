//
//  StartProfileView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 16.01.2022.
//

import SwiftUI

struct StartProfileView: View {

    // MARK: - Properties
    @EnvironmentObject var authViewModel: AuthenticationViewModel

    // MARK: - Init
    var body: some View {
        if authViewModel.hasAccount() {
            ProfileView()
        } else {
            AuthenticationView()
        }
    }
}

struct StartProfileView_Previews: PreviewProvider {
    static var previews: some View {
        StartProfileView()
    }
}
