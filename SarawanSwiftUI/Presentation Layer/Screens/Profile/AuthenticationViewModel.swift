//
//  AuthenticationViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 16.01.2022.
//

import Foundation
import Combine
import KeychainSwift
import CryptoKit

/// TokenResponse(token: "8e6dba7c7bf3997b748db8d1d40ed998ab67cd9b", userId: 21)
final class AuthenticationViewModel: ObservableObject {

    // MARK: - Properties
    // Timer properties
    var timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @Published var defaultTime = 3
    @Published var isActiveTimer = true

    // Properties for TelephoneCodeView
    @Published var isCodeCorrect = true // Проверка кода на правильность
    @Published var isCanPushOTPCodeTap = false // Прокаверка на возможность нажатия на кнопку отправить код для получени токена
    @Published var isCanRepeatPushOTPCode = false // Прокаверка на возможность повторного нажатия на кнопку отправить код
    @Published var sendCodeRepeatCodeQueryValidation = FormValidation() // Валидация нажатия кнопки отправки кода дял получения токена

    // Properties for AuthenticationView
    @Published var phoneValidation = FormValidation() // Валидация поля набора номера телефона
    @Published var agreementValidation = FormValidation() // Валидация нажатия кнопки согласия с соглашением
    @Published var sendCodeOTPButtonValidation = FormValidation() // Валидация нажатия кнопки отправки кода дял получения токена
    @Published var isAgreemantTextTap = false // Проверка нажатия кнопки согласия на обработку персональных данныхы
    @Published var canGetCode = false // Проверка на возможность отправки смс для получения кода

    //
    @Published var phoneNumber = ""
    @Published var isLoggedIn = false


    private var userDefaults = UserDefaults.standard
    private var keychain = KeychainSwift()
    private var token = ""

    @Published var oneTimeCode: String = ""

    struct Config {
        static let onTimeCodeLength = 6 // количество знаков в коде
        static let recommendedLengthPhone = 10 // количество знаков в номере телефона
        static let phoneCharacters = NSPredicate(format:"SELF MATCHES %@","^[0-9+]{0,1}+[0-9]{5,16}$") // условия ввода телефонного номера
    }

    // MARK: - Private Properties
    private let authService: AuthServiceProtocol?
    private var cancellable = Set<AnyCancellable>()

    // MARK: - Init
    init(authService: AuthServiceProtocol?) {
        self.authService = authService
        self.phoneNumber = phoneNumber

        // Properties for TelephoneCodeView
        sendButtonCodeOTPPublisher
            .assign(to: \.sendCodeOTPButtonValidation, on: self)
            .store(in: &cancellable)

        sendButtonCodeOTPPublisher
            .map { sendCodeOTPButtonValidation in
                return sendCodeOTPButtonValidation.success
            }
            .assign(to: \.isCanPushOTPCodeTap, on: self)
            .store(in: &cancellable)

        sendRepeatButtonCodeOTPPublisher
            .assign(to: \.sendCodeRepeatCodeQueryValidation, on: self)
            .store(in: &cancellable)

        sendRepeatButtonCodeOTPPublisher
            .map { sendRepeatCodeOTPButtonValidation in
                return sendRepeatCodeOTPButtonValidation.success
            }
            .assign(to: \.isCanRepeatPushOTPCode, on: self)
            .store(in: &cancellable)

        // Properties for AuthenticationView
        phonePublisher
            .assign(to: \.phoneValidation, on: self)
            .store(in: &cancellable)

        agreementPublisher
            .assign(to: \.agreementValidation, on: self)
            .store(in: &cancellable)

        Publishers.CombineLatest(phonePublisher, agreementPublisher)
            .map { phoneValidation, agreementValidation in
                return phoneValidation.success && agreementValidation.success
            }
            .assign(to: \.canGetCode, on: self)
            .store(in: &cancellable)
    }

    // Properties for AuthenticationView
    private var sendButtonCodeOTPPublisher: AnyPublisher<FormValidation, Never> {
        self.$oneTimeCode
            .debounce(for: 0.2, scheduler: RunLoop.main)
            .map { oneTimeCode in
                if oneTimeCode.isEmpty {
                    return FormValidation(success: false, message: "")
                }
                if oneTimeCode.count < Config.onTimeCodeLength {
                    return FormValidation(success: false, message: "Количество цифр неверно")
                }
                return FormValidation(success: true, message: "")
            }
            .eraseToAnyPublisher()
    }

    private var sendRepeatButtonCodeOTPPublisher: AnyPublisher<FormValidation, Never> {
        self.$defaultTime
            .map { defaultTime in

                if defaultTime != 0 {
                    return FormValidation(success: false, message: "")
                }

                return FormValidation(success: true, message: "")
            }
            .eraseToAnyPublisher()
    }

    /// Properties for TelephoneCodeView
    private var agreementPublisher: AnyPublisher<FormValidation, Never> {
        self.$isAgreemantTextTap
            .map { isAgreemantTextTap in
                if isAgreemantTextTap {
                    return FormValidation(success: true,
                                          message: "")
                } else {
                    return FormValidation(success: false,
                                          message: "Нажмите соглашение о конфиденциальности")
                }
            }
            .eraseToAnyPublisher()
    }

    private var phonePublisher: AnyPublisher<FormValidation, Never> {
        self.$phoneNumber
            .debounce(for: 0.2, scheduler: RunLoop.main)
            .map { phoneNumber in
                if phoneNumber.isEmpty {
                    return FormValidation(success: false, message: "")
                }
                if phoneNumber.count < Config.recommendedLengthPhone {
                    return FormValidation(success: false, message: "Количество цифр неверно")
                }
                if phoneNumber.count > Config.recommendedLengthPhone {
                    return FormValidation(success: false, message: "Слишком много цифр")
                }
                return FormValidation(success: true, message: "")
            }
            .eraseToAnyPublisher()
    }

    // MARK: - Network functions
    /// Получаем токен и сохраняем его в Keychain c помощью номера телефона и полученного кода из смс
    func getToken() {
        authService?.getToken(for: getTelephoneNumber(), code: oneTimeCode)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
//                        self.isActiveTimer = false
                        self.isCodeCorrect = true
                        self.oneTimeCode = ""
                        self.phoneNumber = ""
                        self.isAgreemantTextTap = false
                        print("TODO: Finished")
                    case .failure(let error):
//                        self.isActiveTimer = true

                        self.isCodeCorrect = false
                        print("TODO: Ошибка ", error)
                }
            }, receiveValue: { token in
                print("TODO: \(token)")
                self.createAccount(token: token.token)
            })
            .store(in: &cancellable)
    }

    /// Получаем смс с кодом с помощью номера телефона
    func getSMS() {
        authService?.sendSms(to: getTelephoneNumber())
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("Успешно")
                    case .failure(let error):
                        print("Ошибка ", error)
                }
            }, receiveValue: { text in
                print("Some ", text)
            })
            .store(in: &cancellable)
    }

    /// Добавляем +7 к номеру телефона
    func getTelephoneNumber() -> String {
        return "+7\(phoneNumber)"
    }


    /// Проверка на наличие аккаунта по токену
    /// - Returns: Возвращаем true либо false при наличии либо отсутствии аккаунта
    func hasAccount() -> Bool {
        guard let isHasAccount = keychain.get(AuthKeys.token) else { return false }
//        print("TODO: ", isHasAccount)
//        print("TODO: ", keychain.get(AuthKeys.token))
        return true
    }

    /// Cоздаем аккаунт
    private func createAccount(token: String) {
        guard !hasAccount() else { return }
        let hashedToken = hashToken(token)
        let tokenResult = keychain.set(hashedToken, forKey: AuthKeys.token, withAccess: .accessibleWhenPasscodeSetThisDeviceOnly)

        if tokenResult {
            login()
        } else {
            print("СreateAccount - Что-то не то")
        }
    }

    private func login() {
        userDefaults.set(true, forKey: AuthKeys.isLoggedIn)
        self.isLoggedIn = true
    }


    /// Функция выхода из личного кабинета
    func logout() {
        keychain.delete(AuthKeys.phoneNumber)
        keychain.delete(AuthKeys.token)
        keychain.delete(AuthKeys.salt)
        userDefaults.set(false, forKey: AuthKeys.isLoggedIn)
        self.isLoggedIn = false
    }

    /// Функция определяющая условия запуска таймера
    func getTimeOnTimer() {
        startTimer()
        guard isActiveTimer == true else { return }
        if defaultTime > 0 {
//            self.isCanPushOTPCode = true
//            isCanRepeatPushOTPCode = false
            defaultTime -= 1
        } else if defaultTime == 0 {
//            if isCodeCorrect {
//                isCanPushOTPCode = true
//            }
//            oneTimeCode = ""
//            isCanRepeatPushOTPCode = true
            stopTimer()
        }
    }

    /// Запускаем таймер
    func startTimer() {
        self.timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    }

    /// Останавливаем таймер
    private func stopTimer() {
        self.timer.upstream.connect().cancel()
    }

    /// Функция для дополнительной защиты токена
    /// - Parameters:
    ///   - token: Токен полученный на пользователя
    ///   - reset: Для удаления
    /// - Returns: Вовзращаемый захешированный токен
    private func hashToken(_ token: String, reset: Bool = false) -> String {
        var salt = ""

        if let savedSalt = keychain.get(AuthKeys.salt), !reset {
            salt = savedSalt
        } else {
            let key = SymmetricKey(size: .bits256)
            salt = key.withUnsafeBytes({ Data(Array($0)).base64EncodedString() })
            keychain.set(salt, forKey: AuthKeys.salt)
        }

        guard let data = "\(token)\(salt)".data(using: .utf8) else { return "" }
        let digest = SHA256.hash(data: data)

        return digest.map{String(format: "%02hhx", $0)}.joined()
    }
}
