//
//  AddressProfileView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 13.01.2022.
//

import SwiftUI

struct AddressProfileView: View {

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var profileViewModel: ProfileViewModel

    var body: some View {
        VStack {
            BackNavigationButton(title: "Aдрес") {
                self.presentationMode.wrappedValue.dismiss()
            }

            VStack(spacing: 20) {
                TextField("Город Москва", text: $profileViewModel.firstName)
                    .padding()
                    .background(Color.greenBackground)
                    .cornerRadius(10)

                TextField("Улица", text: $profileViewModel.secondName)
                    .padding()
                    .background(Color.greenBackground)
                    .cornerRadius(10)

                HStack {
                    TextField("Дом", text: $profileViewModel.firstName)
                        .padding()
                        .background(Color.greenBackground)
                        .cornerRadius(10)

                    TextField("Подъезд", text: $profileViewModel.secondName)
                        .padding()
                        .background(Color.greenBackground)
                        .cornerRadius(10)

                    TextField("Квартира", text: $profileViewModel.secondName)
                        .padding()
                        .background(Color.greenBackground)
                        .cornerRadius(10)
                }
            }
            .padding(.horizontal)
            
            Spacer()

            Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Text("Сохранить")
                    .font(.montserratFont(.semibold, size: 16))
                    .foregroundColor(Color.greenApp)
                    .padding(.vertical, 10)
                    .padding(.horizontal, 30)
            }
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.greenApp, lineWidth: 2)
            )
            .cornerRadius(10)
        }
        .padding(.bottom, 90)
        .navigationBarHidden(true)
    }
}

struct AddressProfileView_Previews: PreviewProvider {
    static var previews: some View {
        AddressProfileView()
            .environmentObject(ProfileViewModel())
    }
}
