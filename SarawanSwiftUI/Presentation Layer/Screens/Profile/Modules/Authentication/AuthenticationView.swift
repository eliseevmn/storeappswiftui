//
//  RegistrationVIew.swift
//  SarawanSwiftUI
//
//  Created by MAC on 13.01.2022.
//

import SwiftUI

struct AuthenticationView: View {

    @EnvironmentObject var viewModel: AuthenticationViewModel
    @State var isPresented: Bool = false

    var body: some View {
        NavigationView {
            VStack(spacing: 10) {

                Text("Добро пожаловать!")
                    .font(.montserratFont(.medium, size: 32))
                    .frame(alignment: .center)
                    .frame(maxWidth: .infinity)

                Text("Введите номер телефона, чтобы войти или зарегистрироваться")
                    .font(.montserratFont(.regular, size: 18))
                    .frame(alignment: .center)
                    .frame(maxWidth: .infinity)

                VStack(spacing: 0) {
                    HStack(spacing: 0) {
                        Text("+7")
                            .font(.montserratFont(.medium, size: 18))
                            .padding(.leading, 20)
                        TextField("(___) ___ ___ ___", text: $viewModel.phoneNumber)
                            .font(.montserratFont(.medium, size: 22))
                            .keyboardType(.numberPad)
                            .padding()
                    }
                    .background(Color.greenBackground)
                    .cornerRadius(10)

                    Text(viewModel.phoneValidation.message.isEmpty ?
                         viewModel.agreementValidation.message :
                         viewModel.phoneValidation.message)
                        .frame(height: 50)
                        .foregroundColor(Color.red)

                } // VStack


                Button(action: {
                    isPresented.toggle()
                    viewModel.getSMS()
                }, label: {
                    Text("Получить СМС с кодом")
                        .font(.montserratFont(.semibold, size: 14))
                        .foregroundColor(Color.whiteApp)
                        .padding(.vertical, 15)
                        .padding(.horizontal, 25)
                        .background(!viewModel.canGetCode ?
                                        Color.greenBackground :
                                        Color.greenApp)
                        .cornerRadius(10)
                })
                .disabled(!viewModel.canGetCode)


                HStack {
                    CheckView(title: "Нажимаю на кнопку, я принимаю условия пользовательного соглашения",
                              isChecked: $viewModel.isAgreemantTextTap)
                }
                .padding(.top, 20)
                .padding(.horizontal, 20)

                Spacer()
            }
            .padding(.horizontal)
            .fullScreenCover(isPresented: $isPresented) {
                TelephoneCodeView()
            }
        }
        .navigationBarHidden(true)
        .onTapGesture {
            self.endEditing()
        }
    }

    private func endEditing() {
        UIApplication.shared.endEditing()
    }
}

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

struct RegistrationVIew_Previews: PreviewProvider {
    static var previews: some View {
        AuthenticationView()
            .environmentObject(AuthenticationViewModel(authService: nil))
    }
}

struct CheckView: View {

    var title: String
    @Binding var isChecked: Bool

    var body: some View {
        Button(action: {
            isChecked.toggle()
        } ){
            HStack{
                Image(systemName: isChecked ? "checkmark.square": "square")
                    .font(.montserratFont(.regular, size: 24))
                Text(title)
                    .font(.montserratFont(.medium, size: 14))
            }
        }
        .foregroundColor(.blackApp)
    }
}
