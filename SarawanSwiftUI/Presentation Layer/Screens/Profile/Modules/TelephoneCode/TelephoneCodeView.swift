//
//  TelephoneCodeView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 13.01.2022.
//

import SwiftUI
import Introspect

struct TelephoneCodeView: View {

    // MARK: - Properties
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: AuthenticationViewModel

    // MARK: - Body
    var body: some View {
        VStack(spacing: 10) {

            NavigateDismissButton {
                self.presentationMode.wrappedValue.dismiss()
            }

            Text("Введите код из СМС")
                .font(.montserratFont(.semibold, size: 20))
                .padding(.top, 40)

            PasscodeField(pin: $viewModel.oneTimeCode,
                          isCodeCorrect: $viewModel.isCodeCorrect,
                          handler: { onTimeCode, two in
                viewModel.oneTimeCode = onTimeCode
            })
            .padding(.top, 20)

            Button(action: {
                viewModel.getToken()
                viewModel.isCanPushOTPCodeTap = false

                if viewModel.isCodeCorrect {
                    self.presentationMode.wrappedValue.dismiss()
                }
            }, label: {
                Text("Отправить")
                    .font(.montserratFont(.semibold, size: 20))
                    .foregroundColor(Color.whiteApp)
                    .padding(.vertical, 10)
                    .padding(.horizontal, 20)
                    .background(!viewModel.isCanPushOTPCodeTap ?
                                    Color.greenBackground :
                                    Color.greenApp)
                    .cornerRadius(15)
            })
            .padding(.top, 20)
            .disabled(!viewModel.isCanPushOTPCodeTap)

            VStack(spacing: 5) {
                Text("Код направлен на телефон")
                Text("\(viewModel.getTelephoneNumber())")
            }
            .padding(.top, 10)

            HStack(spacing: 0) {
                Text("Запросить код повторно: через ")
                Text(convertSecondsTiTime(timeInSeconds: viewModel.defaultTime))
                    .onReceive(viewModel.timer) { time in
                        viewModel.getTimeOnTimer()
                    }
            }
            .padding(.top, 10)

            Button(action: {
                viewModel.getToken()
                viewModel.defaultTime = 3
                viewModel.startTimer()
                viewModel.isCodeCorrect = true
                viewModel.oneTimeCode = ""
            }, label: {
                Text("Запросить код повторно")
                    .font(.montserratFont(.semibold, size: 20))
                    .foregroundColor(Color.whiteApp)
                    .padding(.vertical, 10)
                    .padding(.horizontal, 20)
                    .background(!viewModel.isCanRepeatPushOTPCode ?
                                    Color.greenBackground :
                                    Color.greenApp)
                    .cornerRadius(15)
            })
            .padding(.top, 20)
            .disabled(!viewModel.isCanRepeatPushOTPCode)

            Spacer()
        }
        .padding(.horizontal, 20)
        .navigationBarHidden(true)
    } // Body


    /// Функция для преобразования числа в формат минуты:секунды
    /// - Parameter timeInSeconds: целое число
    /// - Returns: возвращает текст нужного формата
    private func convertSecondsTiTime(timeInSeconds: Int) -> String {
        let minutes = timeInSeconds / 60
        let seconds = timeInSeconds % 60
        return String(format: "%02i:%02i", minutes, seconds)
    }
}

// MARK: - PreviewProvider
struct TelephoneCodeView_Previews: PreviewProvider {
    static var previews: some View {
        TelephoneCodeView()
    }
}


