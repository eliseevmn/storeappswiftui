//
//  PasscodeField.swift
//  SarawanSwiftUI
//
//  Created by MAC on 15.01.2022.
//

import SwiftUI

struct PasscodeField: View {

    var maxDigits: Int = 6
    var label = "Введите код из СМС"

    @Binding var pin: String
    @State var showPin = true
    @State var isDisabled = false
    @Binding var isCodeCorrect: Bool

    var handler: (String, (Bool) -> Void) -> Void

    public var body: some View {
        VStack(spacing: 20) {
            ZStack {
                pinDots
                backgroundField
            }
        }
    }

    private var pinDots: some View {
        HStack(spacing: 10) {
            ForEach(0..<maxDigits) { index in
                ZStack {
                    Text(self.getStringNumber(at: index))
                        .foregroundColor(Color.blackApp)
                        .font(.montserratFont(.semibold, size: 20))
                }
                .frame(width: 32, height: 48)
                .background(Color.greyApp)
                .overlay(RoundedRectangle(cornerRadius: 0)
                            .stroke(lineWidth: 1)
                            .foregroundColor(isCodeCorrect ? .greyApp : .red))

            }
        }
    }

    private var backgroundField: some View {

        let boundPin = Binding<String>(
            get: {
                self.pin
            }, set: { newValue in
                self.pin = newValue
                self.submitPin()
            })

        return TextField("", text: boundPin, onCommit: submitPin)
           .accentColor(.clear)
           .foregroundColor(.clear)
           .keyboardType(.numberPad)
           .disabled(isDisabled)
           .introspectTextField { textField in
               textField.becomeFirstResponder()
           }
    }

//    private var showPinStack: some View {
//        HStack {
//            Spacer()
//            if !pin.isEmpty {
//                showPinButton
//            }
//        }
//        .frame(height: 50)
//        .padding([.trailing])
//    }

//    private var showPinButton: some View {
//        Button(action: {
//            self.showPin.toggle()
//        }, label: {
//            if self.showPin {
//                Image(systemName: "eye.slash.fill")
//                    .foregroundColor(.primary)
//            } else {
//                Image(systemName: "eye.fill")
//                    .foregroundColor(.primary)
//            }
//        })
//    }

    private func submitPin() {
        guard !pin.isEmpty else {
            showPin = false
            return
        }

        if pin.count == maxDigits {
            isDisabled = true

            handler(pin) { isSuccess in
                if isSuccess {
                    print("pin matched, go to next page, no action to perfrom here")
                } else {
                    pin = ""
                    isDisabled = false
                    print("this has to called after showing toast why is the failure")
                }
            }
        }

        // this code is never reached under  normal circumstances. If the user pastes a text with count higher than the
        // max digits, we remove the additional characters and make a recursive call.
        if pin.count > maxDigits {
            pin = String(pin.prefix(maxDigits))
            submitPin()
        }
    }

    private func getStringNumber(at index: Int) -> String {
        if index >= self.pin.count {
            return ""
        }
        if self.showPin {
            print("TODO: ", pin)
            return self.pin.digits[index].numberString
        }
        return ""
    }
}

// MARK: - PreviewProvider
struct PasscodeField_Previews: PreviewProvider {
    static var previews: some View {
        PasscodeField(maxDigits: 6, pin: .constant(""), showPin: false, isDisabled: false, isCodeCorrect: .constant(true), handler: { _, _ in })
    }
}
