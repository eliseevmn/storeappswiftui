//
//  ProfileView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 12.01.2022.
//

import SwiftUI

struct ProfileView: View {

    @EnvironmentObject var profileModel: ProfileViewModel
    @EnvironmentObject var authModel: AuthenticationViewModel
    
    var body: some View {
        NavigationView {

            ScrollView(.vertical) {

                VStack {

                    TitleMainView(titleName: "Профиль")

                    VStack {
                        NavigationLink(destination: NameProfileView()) {
                            HStack {
                                Text(profileModel.fullNameUser())
                                    .font(.montserratFont(.regular, size: 16))
                                Spacer()
                                Image("icArrowRight")
                            } // HStack
                        } // NavigationLink
                        .foregroundColor(Color.blackApp)
                        .padding(.vertical, 10)

                        NavigationLink(destination: AddressProfileView()) {
                            HStack {
                                Text("Aдрес")
                                    .font(.montserratFont(.regular, size: 16))
                                    .lineLimit(nil)
                                Spacer()
                                Image("icArrowRight")
                            } // HStack
                        } // NavigationLink
                        .foregroundColor(Color.blackApp)
                        .padding(.vertical, 10)
                    }

                    HStack {
                        Button(action: {
                            authModel.logout()
                        }) {
                            Text("Выйти")
                                .font(.montserratFont(.medium, size: 14))
                                .foregroundColor(Color.greyApp)
                        }
                        Spacer()
                    }
                    .padding(.top, 40)

                }
                .padding()
            }
            .navigationBarHidden(true)
        } // NavigationView

    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
            .environmentObject(ProfileViewModel())
    }
}
