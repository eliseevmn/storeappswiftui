//
//  InfoNavigateRow.swift
//  SarawanSwiftUI
//
//  Created by MAC on 07.01.2022.
//

import SwiftUI

struct InfoNavigateRow<Destination: View>: View {

    // MARK: - Properties
    let destination : Destination
    let title: String
    let icon: String

    // MARK: - Body
    var body: some View {
        NavigationLink(destination: destination) {
            HStack {
                Text(title)
                    .font(.montserratFont(.regular, size: 16))
                Spacer()
                Image(icon)
            } // HStack
        } // NavigationLink
        .foregroundColor(Color.blackApp)
        .padding(.vertical, 10)
    } // MARK: - Body
}
