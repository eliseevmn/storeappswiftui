//
//  AboutProjectBenefitsView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 08.01.2022.
//

import SwiftUI

struct AboutProjectBenefitsView: View {

    // MARK: - Properties
    let benefitsModel: [BenefitModel]

    // MARK: - Body
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(benefitsModel, id:\.self) { model in
                HStack(alignment: .center, spacing: 20) {
                    Image(model.icon)
                        .resizable()
                        .aspectRatio(1, contentMode: .fit)
                        .frame(width: 60, height: 60)
                        .foregroundColor(Color.greenApp)
                    
                    VStack(alignment: .leading, spacing: 10) {
                        Text(model.title)
                            .font(.montserratFont(.semibold, size: 18))
                        Text(model.description)
                            .font(.montserratFont(.regular, size: 16))
                            .lineLimit(nil)
                            .multilineTextAlignment(.leading)
                            .fixedSize(horizontal: false, vertical: true)
                    } // VStack
                } // HSTack
            } // ForEach
        } // VStack
    } // Body
}
