//
//  AboutProjectViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 08.01.2022.
//

import Foundation

final class AboutProjectViewModel: ObservableObject {

    // MARK: - Properties
    var benefits: [BenefitModel] = [
        BenefitModel(icon: "icDairy",
                     title: Texts.AboutProject.wideAssortment,
                     description: Texts.AboutProject.wideAssortmentDescription),
        BenefitModel(icon: "icVegetable",
                     title: Texts.AboutProject.freshFood,
                     description: Texts.AboutProject.freshFoodDescription),
        BenefitModel(icon: "icMoneyBox",
                     title: Texts.AboutProject.costSaving,
                     description: Texts.AboutProject.costSavingDescription),
        BenefitModel(icon: "icCar",
                     title: Texts.AboutProject.fastShipping,
                     description: Texts.AboutProject.fastShippingDescription)
    ]
}
