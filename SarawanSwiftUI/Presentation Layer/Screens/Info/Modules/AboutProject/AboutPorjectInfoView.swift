//
//  AboutPorjectInfoView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 08.01.2022.
//

import SwiftUI

struct AboutPorjectInfoView: View {

    // MARK: - Body
    var body: some View {
        VStack(spacing: 10) {
            HStack {
                Text(Texts.AboutProject.about)
                    .font(.montserratFont(.medium, size: 28))
                Spacer()
            } // HStack
            Text(Texts.AboutProject.info)
                .font(.montserratFont(.regular, size: 16))
        } // VStack
        .padding()
        .background(Color.greenBackground)
    } // Body
}

// MARK: - PreviewProvider
struct AboutPorjectInfoView_Previews: PreviewProvider {
    static var previews: some View {
        AboutPorjectInfoView()
    }
}
