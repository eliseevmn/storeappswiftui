//
//  AboutProjectView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 08.01.2022.
//

import SwiftUI

struct AboutProjectView: View {

    // MARK: - Properties
    let title: String
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: AboutProjectViewModel

    // MARK: - Body
    var body: some View {
        VStack(spacing: 0) {
            BackNavigationButton(title: title,
                                 dismissButton: {
                presentationMode.wrappedValue.dismiss()
            }) // BackNavigationButton

            ScrollView(.vertical, showsIndicators: false) {

                VStack {
                    AboutPorjectInfoView()
                    HStack {
                        Text(Texts.AboutProject.benefits)
                            .font(.montserratFont(.semibold, size: 24))
                        Spacer()
                    }
                    AboutProjectBenefitsView(benefitsModel: viewModel.benefits)
                    Spacer()
                } // VStack
                .padding(.horizontal, 20)
            } // ScrollView
        } // VStack
        .navigationBarHidden(true)
    } // MARK: - Body
}

// MARK: - PreviewProvider
struct AboutProjectView_Previews: PreviewProvider {
    static var previews: some View {
        AboutProjectView(title: "Назад")
            .environmentObject(AboutProjectViewModel())
    }
}
