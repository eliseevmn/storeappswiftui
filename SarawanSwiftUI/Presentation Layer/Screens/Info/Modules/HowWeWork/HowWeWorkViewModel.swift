//
//  HowWeWorkViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 07.01.2022.
//

import Foundation

final class HowWeWorkViewModel: ObservableObject {

    // MARK: - Properties
    var howWeWorkModels: [HowWeWorkModel] = [
        HowWeWorkModel(idNumber: 1,
                       icon: "icList",
                       description: Texts.HowWeWorkInfo.chooseProduct),
        HowWeWorkModel(idNumber: 2,
                       icon: "icCar",
                       description: Texts.HowWeWorkInfo.delivery),
        HowWeWorkModel(idNumber: 3,
                       icon: "icCart",
                       description: Texts.HowWeWorkInfo.subscription)
    ]
}
