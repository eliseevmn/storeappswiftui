//
//  HowWeWorkRow.swift
//  SarawanSwiftUI
//
//  Created by MAC on 07.01.2022.
//

import SwiftUI

struct HowWeWorkRowView: View {

    // MARK: - Properties
    let model: HowWeWorkModel

    // MARK: - Body
    var body: some View {
        VStack {

            VStack {
                Text("\(model.idNumber)")
                    .font(Font.montserratFont(.regular, size: 20))
                    .frame(width: 55, height: 55, alignment: .center)
                    .foregroundColor(Color.whiteApp)
                    .background(Color.greenApp)
                    .clipShape(Circle())
            } // VStack
            .frame(height: 0)
            .zIndex(1)

            VStack {
                Image(model.icon)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 60, height: 60)
                    .padding(.top, 40)
                Text(model.description)
                    .lineLimit(100)
                    .font(Font.montserratFont(.regular, size: 14))
                    .padding(.horizontal, 10)
                    .padding(.bottom, 20)
            } // VStack
            .frame(maxWidth: .infinity)
            .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(lineWidth: 1)
                        .foregroundColor(Color.greyApp))
        } // VStack
    } // Body
}

// MARK: - PreviewProvider
struct HowWeWorkRowView_Previews: PreviewProvider {
    static var previews: some View {
        HowWeWorkRowView(model: HowWeWorkModel(idNumber: 1,
                                               icon: "icList",
                                               description: "Выберите продукты в каталоге. Система сама подберет вам самые выгодные предложения."))
    }
}
