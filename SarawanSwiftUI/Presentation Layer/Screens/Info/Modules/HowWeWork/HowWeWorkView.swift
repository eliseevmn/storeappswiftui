//
//  HowWeWorkView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 07.01.2022.
//

import SwiftUI

struct HowWeWorkView: View {

    // MARK - Properties
    @EnvironmentObject var viewModel: HowWeWorkViewModel
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    let titleNavigationBar: String

    // MARK: - Body
    var body: some View {
        VStack {
            BackNavigationButton(title: titleNavigationBar) {
                presentationMode.wrappedValue.dismiss()
            } // BackNavigationButton

            ScrollView(.vertical) {
                ForEach(viewModel.howWeWorkModels, id:\.self) { model in
                    HowWeWorkRowView(model: model)
                        .frame(maxWidth: .infinity)
                        .padding(.top, 30)
                        .padding(.bottom, 20)
                        .padding(.horizontal, 80)
                }
            } // ScrollView
        } // VStack
        .navigationBarHidden(true)
    }
}

// MARK: - PreviewProvider
struct HowWeWorkView_Previews: PreviewProvider {
    static var previews: some View {
        HowWeWorkView(titleNavigationBar: "Назад")
            .environmentObject(HowWeWorkViewModel())
    }
}
