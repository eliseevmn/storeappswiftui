//
//  SupportProjectView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 08.01.2022.
//

import SwiftUI

struct SupportProjectView: View {

    // MARK: - Properties
    let title: String
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var viewModel: SupportProjectViewModel

    // MARK: - Body
    var body: some View {
        VStack {
            BackNavigationButton(title: title,
                                 dismissButton: {
                presentationMode.wrappedValue.dismiss()
            }) // BackNavigationButton

            HStack {
                Text(viewModel.supportTelephone)
                Spacer()
            } // HStack
            .padding()

            Spacer()
        } // VStack
        .navigationBarHidden(true)
    } // Body
}

// MARK: - PreviewProvider
struct SupportProjectView_Previews: PreviewProvider {
    static var previews: some View {
        SupportProjectView(title: "Назад")
            .environmentObject(SupportProjectViewModel())
    }
}
