//
//  SupportProjectViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 08.01.2022.
//

import Foundation

final class SupportProjectViewModel: ObservableObject {

    // MARK: - Properties
    let supportTelephone = Texts.SupportProject.telephoneNumber
}
