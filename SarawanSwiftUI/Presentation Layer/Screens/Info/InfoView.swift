//
//  InfoView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 05.01.2022.
//

import SwiftUI

struct InfoView: View {

    // MARK: - Properties
    @EnvironmentObject var infoViewModel: InfoViewModel

    // MARK: - Body
    var body: some View {
        NavigationView {
            VStack {
                TitleMainView(titleName: "Инфо")

                ForEach(infoViewModel.infoSettingTypeCells, id: \.self) { cellType in
                    switch cellType {
                        case .howWeWorkInfo(let icon, let title):
                            InfoNavigateRow(destination: HowWeWorkView(titleNavigationBar: title),
                                            title: title,
                                            icon: icon)
                        case .supportProjectInfo(let icon, let title):
                            InfoNavigateRow(destination: SupportProjectView(title: title),
                                            title: title,
                                            icon: icon)
                        case .aboutProjectInfo(let icon, let title):
                            InfoNavigateRow(destination: AboutProjectView(title: title),
                                            title: title,
                                            icon: icon)
                    }
                } // ForEach
                Spacer()
            } // VStack
            .ignoresSafeArea()
            .navigationBarHidden(true)
            .padding(.top, 60)
            .padding(.horizontal)
        } // NavigationView
    } // Body
}

// MARK: - PreviewProvider
struct InfoView_Previews: PreviewProvider {
    static var previews: some View {
        InfoView()
    }
}
