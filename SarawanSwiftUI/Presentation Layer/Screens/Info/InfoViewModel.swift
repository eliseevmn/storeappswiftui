//
//  InfoViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 05.01.2022.
//

import Foundation
import SwiftUI

enum InfoSettingCellType: Hashable {
    case howWeWorkInfo(icon: String, title: String)
    case supportProjectInfo(icon: String, title: String)
    case aboutProjectInfo(icon: String, title: String)
}

class InfoViewModel: ObservableObject {

    // MARK: - Properties
    let infoSettingTypeCells: [InfoSettingCellType] = [
        .howWeWorkInfo(icon: "icArrowRight",title: Texts.CommonInfo.howWeWork),
        .supportProjectInfo(icon: "icArrowRight", title: Texts.CommonInfo.supportProject),
        .aboutProjectInfo(icon: "icArrowRight", title: Texts.CommonInfo.aboutProject)
    ]
}
