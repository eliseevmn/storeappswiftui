//
//  ProductCardView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 10.01.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProductCardView: View {

    // MARK: - properties
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var productViewModel: ProductCardViewModel
    @Binding var idProduct: Int?

    // MARK: - Body
    var body: some View {
        switch productViewModel.stateProduct {
            case .isLoading:
                ProgressView()
                    .onAppear {
                        productViewModel.fetchProductById(id: idProduct ?? 0)
                        productViewModel.fetchSimilarProduct(id: idProduct ?? 0)
                    }
            case .success(let product):
                createContent(product: product)
            case .failure(let error):
                Text(error.localizedDescription)
        } // Switch
    } // Body

    // MARK: - Helpers functions
    private func createContent(product: ProductModel) -> some View {
        ScrollView(.vertical) {
            VStack(spacing: 15) {

                NavigateDismissButton(onTapDissmissButton: {
                    presentationMode.wrappedValue.dismiss()
                })

                Text(product.nameFood)
                    .modifier(TitleTextModifier(font: .montserratFont(.medium, size: 28)))

                ProductCardImagesSection(product: product)

                ProductCardPriceRow(product: product)

                Text("Минимальная цена: " + product.storeName)
                    .modifier(TitleTextModifier(font: .montserratFont(.regular, size: 12)))

                ProductCardAddToBasketButton(onTapToBasketButton: {
                    // Нажатие на добавлению в корзину
                })

                ProductCardStoresSection(product: product, onTapChangeProductCountButton: {
                    // Нажатие на изменение кол-ва товаров
                }, productCont: $productViewModel.productCount)

                VStack(spacing: 15) {
                    Text("С этим товаром смотрят:")
                        .modifier(TitleTextModifier(font: .montserratFont(.medium, size: 18)))

                    HStack(spacing: 15) {
                        ForEach(productViewModel.stateSimilarProducts.prefix(2)) { product in
                            ProductCellView(product: product, tapBuyButton: nil, isShowDiscount: false)
                        } // ForEach
                    } // HStack

                    Text("Описание")
                        .modifier(TitleTextModifier(font: .montserratFont(.medium, size: 16)))

                    Text(product.descriptionProduct)
                        .modifier(TitleTextModifier(font: .montserratFont(.regular, size: 16)))
                }// VStack
                .padding(0)

            } // VStack
            .padding(.horizontal)
        } // ScrollView
    } // CreateContent
}
