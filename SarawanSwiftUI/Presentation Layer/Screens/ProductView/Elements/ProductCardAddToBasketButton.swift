//
//  ProductCardAddToBasketButton.swift
//  SarawanSwiftUI
//
//  Created by MAC on 12.01.2022.
//

import SwiftUI

struct ProductCardAddToBasketButton: View {

    // MARK: - Properties
    var onTapToBasketButton: (() -> Void)

    // MARK: - Body
    var body: some View {
        Button(action: onTapToBasketButton) {
            Text("Добавить в корзину")
                .font(.montserratFont(.semibold, size: 16))
                .frame(maxWidth: .infinity)
                .padding(.vertical, 10)
        }
        .background(Color.green)
        .foregroundColor(Color.white)
        .cornerRadius(10)
        .padding(.horizontal, 60)
    } // Body
}

// MARK: - PreviewProvider
struct ProductCardAddToBasketButton_Previews: PreviewProvider {
    static var previews: some View {
        ProductCardAddToBasketButton(onTapToBasketButton: {})
    }
}
