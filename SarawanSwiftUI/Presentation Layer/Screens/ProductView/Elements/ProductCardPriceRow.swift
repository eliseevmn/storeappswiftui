//
//  ProductCardPriceSection.swift
//  SarawanSwiftUI
//
//  Created by MAC on 12.01.2022.
//

import SwiftUI

struct ProductCardPriceRow: View {

    // MARK: - Properties
    let product: ProductModel

    // MARK: - Body
    var body: some View {
        HStack {
            Text(product.priceFood)
                .modifier(TitleTextModifier(font: .montserratFont(.medium, size: 28)))
            Spacer()
            Text(product.unitQuantity + " " + product.priceType)
                .modifier(TitleTextModifier(font: .montserratFont(.regular, size: 14)))
        } // HStack
    } // Body
}

// MARK: - PreviewProvider
struct ProductCardPriceRow_Previews: PreviewProvider {
    static var previews: some View {
        ProductCardPriceRow(product: ProductModel.placeholder)
    }
}
