//
//  ProductCardStoresSection.swift
//  SarawanSwiftUI
//
//  Created by MAC on 12.01.2022.
//

import SwiftUI

struct ProductCardStoresSection: View {

    // MARK: - Properties
    let product: ProductModel
    let onTapChangeProductCountButton: (() -> Void)
    @Binding var productCont: Int

    // MARK: - Body
    var body: some View {
        VStack(spacing: 0) {
            Text("Цена в других магазинах:")
                .modifier(TitleTextModifier(font: .montserratFont(.medium, size: 16)))
                .padding(.bottom, 10)

//            ScrollView {
                ForEach(product.storesFood) { store in
                    HStack {
                        Text(store.name)
                            .modifier(TitleTextModifier(font: .montserratFont(.regular, size: 16)))

                        Spacer()

                        Text(store.price + " ₽")
                            .foregroundColor(Color.greyApp)
                            .padding(.trailing, 20)


                        HStack(spacing: 5) {
                            Button(action: onTapChangeProductCountButton) {
                                Image(systemName: "minus")
                                    .foregroundColor(Color.greenApp)
                                    .font(.system(size: 18))
                            }

                            Text("\(productCont)")
                                .font(.montserratFont(.semibold, size: 22))
                                .foregroundColor(Color.greenApp)
                                .frame(width: 32, height: 32)

                            Button(action: onTapChangeProductCountButton) {
                                Image(systemName: "plus")
                                    .foregroundColor(Color.greenApp)
                                    .font(.system(size: 18))
                            }
                        } // HStack
                    }
                } // ForEach
//            } // ScrollView
        }
        .padding()
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                    .stroke(lineWidth: 1)
        )

    }
}

struct ProductCardStoresSection_Previews: PreviewProvider {
    static var previews: some View {
        ProductCardStoresSection(product: ProductModel.placeholder,
                                 onTapChangeProductCountButton: {},
                                 productCont: .constant(0))
    }
}
