//
//  ProductCardImagesSection.swift
//  SarawanSwiftUI
//
//  Created by MAC on 12.01.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProductCardImagesSection: View {

    // MARK: - Properties
    let product: ProductModel

    // MARK: - Body
    var body: some View {
        HStack {
            WebImage(url: URL(string: product.imageFood))
                .placeholder(Image("icDefaultFood"))
                .resizable()
                .aspectRatio(contentMode: .fit)

            VStack {
                ForEach(0..<product.imagesFood.count) { index in
                    if product.imagesFood.count < 2 {
                        VStack {
                            WebImage(url: URL(string: product.imageFood))
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                            Spacer()
                        }
                    }
                    if product.imagesFood.count < 3 && product.imagesFood.count >= 2 {
                        VStack {
                            WebImage(url: URL(string: product.imagesFood[index].imageURL))
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                            Spacer()
                        }
                    }
                    if product.imagesFood.count > 2 {
                        WebImage(url: URL(string: product.imagesFood[index].imageURL))
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                    }
                } // ForEach
            } // VStack
            .frame(width: 100)
        } // HStack
    } // Body
}

// MARK: - PreviewProvider
struct ProductCardImagesSection_Previews: PreviewProvider {
    static var previews: some View {
        ProductCardImagesSection(product: ProductModel.placeholder)
    }
}
