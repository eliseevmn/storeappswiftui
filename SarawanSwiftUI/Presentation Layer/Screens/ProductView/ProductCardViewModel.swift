//
//  ProductCardViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 10.01.2022.
//

import Foundation
import Combine

final class ProductCardViewModel: ObservableObject {

    // MARK: - State
    enum State<T> {
        case isLoading
        case success(T)
        case failure(Error)
    }

    // MARK: - Properties
    @Published var stateProduct: State<ProductModel>
    @Published var stateSimilarProducts: [ProductModel] = []
    @Published var productCount: Int = 0
    private let productService: ProductServiceProtocol?
    private var cancelable = Set<AnyCancellable>()

    // MARK: - Init
    init(productService: ProductServiceProtocol?) {
        self.productService = productService
        self.stateProduct = .isLoading
    }

    // MARK: - Helpers functions
    func fetchProductById(id: Int) {
        self.productService?
            .getProduct(id: id)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO: fetchProductById успешно")
                    case .failure(let error):
                        self.stateProduct = .failure(error)
                }
            }, receiveValue: { product in
                self.stateProduct = .success(ProductModel(productModel: product))
            })
            .store(in: &self.cancelable)
    }

    func fetchSimilarProduct(id: Int) {
        self.productService?
            .getProductsByTypeFilters(isPopularProducts: nil,
                                      nameFood: nil,
                                      categoryId: nil,
                                      filters: nil,
                                      isDiscountProducts: nil,
                                      similarProduct: id,
                                      orderingPrice: false,
                                      storeId: nil)
            .map(\.results)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO: fetchSimilarProduct успешно")
                    case .failure(let error):
                        print(error.localizedDescription)
                }
            }, receiveValue: { products in
                self.stateSimilarProducts = products.shuffled().map({ ProductModel(productModel: $0) })
            })
            .store(in: &self.cancelable)
    }
}
