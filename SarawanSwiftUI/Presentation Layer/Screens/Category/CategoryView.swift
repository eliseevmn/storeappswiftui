//
//  CategoryView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 24.01.2022.
//

import SwiftUI

struct CategoryView: View {

    // MARK: - Properties
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var categoryViewModel: CategoryViewModel
    @EnvironmentObject var productViewModel: ProductCardViewModel
    @State private var isPresentedCardCell = false
    @State private var idProduct: Int? = 0
    var typeQuery: TypeFoodQuery
    @Binding var foodName: String
    @State private var filterType: FilterType? = .popularFilter

    // MARK: - Body
    var body: some View {
        switch categoryViewModel.state {
            case .isLoading:
                createContent(products: ProductModel.listRecommentedPlaceholders)
                    .onAppear {
                        categoryViewModel.changeTypeFoodQuery(typeFoodQuery: typeQuery, filterType: .popularFilter)
                    }
                    .redacted(reason: .placeholder)
            case .success(let products):
                createContent(products: products)
            case .failure(let error):
                Text(error.localizedDescription)
        }
    }

    // MARK: - Helpers functions
    private func createContent(products: [ProductModel]) -> some View {
        ZStack {

            CategoryGridView(products: products,
                             idProduct: $idProduct,
                             isPresentedCardCell: $isPresentedCardCell)

            CategoryHeaderView(foodName: $foodName,
                               filterType: $filterType,
                               onDismissButtonTapped: {
                self.presentationMode.wrappedValue.dismiss()
            }) { filterType in
                self.categoryViewModel.changeTypeFoodQuery(typeFoodQuery: typeQuery, filterType: filterType)
            }
        } // ZStack
        .edgesIgnoringSafeArea(.top)
        .navigationBarHidden(true)
        .background(Color.whiteApp)
        .fullScreenCover(isPresented: $isPresentedCardCell) {
            ProductCardView(idProduct: $idProduct)
                .onAppear {
                    productViewModel.stateProduct = .isLoading
                }
        }
    }
}
