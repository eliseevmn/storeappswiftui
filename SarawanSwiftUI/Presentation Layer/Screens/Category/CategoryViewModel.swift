//
//  CategoryViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 24.01.2022.
//

import Foundation
import Combine

final class CategoryViewModel: ObservableObject {

    // MARK: - State
    enum State {
        case isLoading
        case success([ProductModel])
        case failure(Error)
    }

    // MARK: - Properties
    @Published var state: State
    private let productService: ProductServiceProtocol?
    private var cancelable = Set<AnyCancellable>()

    // MARK: - Init
    init(productService: ProductServiceProtocol?) {
        self.productService = productService
        self.state = .isLoading
    }

    // MARK: - Helpers functions

    /// Функция получения продуктов в зависимости от типа перехода на экран Продуктов
    /// - Parameters:
    ///   - typeFoodQuery: выбор типа перехода на экран продуктов
    ///   - filterType: выбор типа сортировки
    func changeTypeFoodQuery(typeFoodQuery: TypeFoodQuery,
                             filterType: FilterType) {
        switch typeFoodQuery {
            case .productsPopular(let catalogFoodModel):
                changeTypeFilterByCategoryId(filterType: filterType,
                                             categoryId: catalogFoodModel.id ?? 0)
            case .productsBySearchName(foodName: let foodName):
                changeTypeFilterByNameFood(filterType: filterType,
                                           nameFood: foodName)
            case .productsByStore(storeModel: let storeModel):
                changeTypeFilterByStoreId(filterType: filterType,
                                          storeId: storeModel.id ?? 0)
        }
    }

    /// Функция для получения продуктов в зависимости от типа сортировки в случае поиска через наименование продукта
    /// - Parameters:
    ///   - filterType: выбор типа сортировки
    ///   - nameFood: наименование продукта
    private func changeTypeFilterByNameFood(filterType: FilterType, nameFood: String) {
        switch filterType {
            case .upPriceFilter:
                getProductsTypeFiltersByNameFood(isPopularProducts: "",
                                                 nameFood: nameFood,
                                                 typeFilter: .upPrice,
                                                 orderingPrice: true)
            case .downPriceFilter:
                getProductsTypeFiltersByNameFood(isPopularProducts: "",
                                                 nameFood: nameFood,
                                                 typeFilter: .downPrice,
                                                 orderingPrice: false)
            case .alphabetFilter:
                getProductsTypeFiltersByNameFood(isPopularProducts: "true",
                                                 nameFood: nameFood,
                                                 typeFilter: .name,
                                                 orderingPrice: nil)
            case .upDiscountFilter:
                getProductsTypeFiltersByNameFood(isPopularProducts: "true",
                                                 nameFood: nameFood,
                                                 typeFilter: .upDiscount,
                                                 orderingPrice: nil)
            case .downDiscountFilter:
                getProductsTypeFiltersByNameFood(isPopularProducts: "true",
                                                 nameFood: nameFood,
                                                 typeFilter: .downDiscount,
                                                 orderingPrice: nil)
            case .popularFilter:
                getProductsTypeFiltersByNameFood(isPopularProducts: "true",
                                                 nameFood: nameFood,
                                                 typeFilter: nil,
                                                 orderingPrice: nil)
        }
    }

    /// Функция для получения продуктов в зависимости от типа сортировки в случае получения продуктов через id главного каталога продуктов
    /// - Parameters:
    ///   - filterType: выбор типа сортировки
    ///   - categoryId: номер id главного католога продуктов
    private func changeTypeFilterByCategoryId(filterType: FilterType, categoryId: Int) {
        switch filterType {
            case .upPriceFilter:
                getProductsTypeFiltersByCatalogId(isPopularProducts: "",
                                                  categoryId: categoryId,
                                                  typeFilter: .upPrice,
                                                  orderingPrice: true)
            case .downPriceFilter:
                getProductsTypeFiltersByCatalogId(isPopularProducts: "",
                                                  categoryId: categoryId,
                                                  typeFilter: .downPrice,
                                                  orderingPrice: false)
            case .alphabetFilter:
                getProductsTypeFiltersByCatalogId(isPopularProducts: "true",
                                                  categoryId: categoryId,
                                                  typeFilter: .name,
                                                  orderingPrice: nil)
            case .upDiscountFilter:
                getProductsTypeFiltersByCatalogId(isPopularProducts: "true",
                                                  categoryId: categoryId,
                                                  typeFilter: .upDiscount,
                                                  orderingPrice: nil)
            case .downDiscountFilter:
                getProductsTypeFiltersByCatalogId(isPopularProducts: "true",
                                                  categoryId: categoryId,
                                                  typeFilter: .downDiscount,
                                                  orderingPrice: nil)
            case .popularFilter:
                getProductsTypeFiltersByCatalogId(isPopularProducts: "true",
                                                  categoryId: categoryId,
                                                  typeFilter: nil,
                                                  orderingPrice: nil)
        }
    }

    /// Функция для получения продуктов в зависимости от типа сортировки в случае получения продуктов через id магазина продуктов
    /// - Parameters:
    ///   - filterType: выбор типа сортировки
    ///   - categoryId: номер id магазина продуктов
    private func changeTypeFilterByStoreId(filterType: FilterType, storeId: Int) {
        switch filterType {
            case .upPriceFilter:
                getProductsTypeFiltersStoreId(isPopularProducts: "",
                                              storeId: storeId,
                                              typeFilter: .upPrice,
                                              orderingPrice: true)
            case .downPriceFilter:
                getProductsTypeFiltersStoreId(isPopularProducts: "",
                                              storeId: storeId,
                                              typeFilter: .downPrice,
                                              orderingPrice: false)
            case .alphabetFilter:
                getProductsTypeFiltersStoreId(isPopularProducts: "true",
                                              storeId: storeId,
                                              typeFilter: .name,
                                              orderingPrice: nil)
            case .upDiscountFilter:
                getProductsTypeFiltersStoreId(isPopularProducts: "true",
                                              storeId: storeId,
                                              typeFilter: .upDiscount,
                                              orderingPrice: nil)
            case .downDiscountFilter:
                getProductsTypeFiltersStoreId(isPopularProducts: "true",
                                              storeId: storeId,
                                              typeFilter: .downDiscount,
                                              orderingPrice: nil)
            case .popularFilter:
                getProductsTypeFiltersStoreId(isPopularProducts: "true",
                                              storeId: storeId,
                                              typeFilter: nil,
                                              orderingPrice: nil)
        }
    }

    /// Функция для получения продуктов из сети по наименованию продукта
    /// - Parameters:
    ///   - isPopularProducts: популярность продуктв
    ///   - nameFood: наименование продукта
    ///   - typeFilter: тип сортировки
    ///   - orderingPrice: сортировка по цене
    private func getProductsTypeFiltersByNameFood(isPopularProducts: String?,
                                                  nameFood: String?,
                                                  typeFilter: FiltersParameters?,
                                                  orderingPrice: Bool?) {
        productService?
            .getProductsByTypeFilters(isPopularProducts: isPopularProducts,
                                      nameFood: nameFood,
                                      categoryId: nil,
                                      filters: typeFilter,
                                      isDiscountProducts: nil,
                                      similarProduct: nil,
                                      orderingPrice: orderingPrice,
                                      storeId: nil)
            .receive(on: RunLoop.main)
            .map(\.results)
            .compactMap({ products in
                return products.map({ ProductModel(productModel: $0) })
            })
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO: fetch filter Products успешно")
                    case .failure(let error):
                        self.state = .failure(error)
                }
            }, receiveValue: { products in
                let removeDuplicatesArray = products.unique()
                self.state = .success(removeDuplicatesArray)
            }).store(in: &cancelable)
    }

    /// Функция для получения продуктов из сети по id главного каталога продуктов
    /// - Parameters:
    ///   - isPopularProducts: популярность продуктв
    ///   - categoryId: номер id главного католога продуктов
    ///   - typeFilter: тип сортировки
    ///   - orderingPrice: сортировка по цене
    private func getProductsTypeFiltersByCatalogId(isPopularProducts: String?,
                                                   categoryId: Int,
                                                   typeFilter: FiltersParameters?,
                                                   orderingPrice: Bool?) {
        productService?
            .getProductsByTypeFilters(isPopularProducts: isPopularProducts,
                                      nameFood: nil,
                                      categoryId: "\(categoryId)",
                                      filters: typeFilter,
                                      isDiscountProducts: nil,
                                      similarProduct: nil,
                                      orderingPrice: orderingPrice,
                                      storeId: nil)
            .receive(on: RunLoop.main)
            .map(\.results)
            .compactMap({ products in
                return products.map({ ProductModel(productModel: $0) })
            })
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO: fetch filter Products успешно")
                    case .failure(let error):
                        self.state = .failure(error)
                    }
            }, receiveValue: { products in
                let removeDuplicatesArray = products.unique()
                self.state = .success(removeDuplicatesArray)
            }).store(in: &cancelable)
    }

    /// Функция для получения продуктов из сети по id магазина
    /// - Parameters:
    ///   - isPopularProducts: популярность продуктв
    ///   - storeId: номер id магазина продуктов
    ///   - typeFilter: тип сортировки
    ///   - orderingPrice: сортировка по цене
    private func getProductsTypeFiltersStoreId(isPopularProducts: String?,
                                               storeId: Int,
                                               typeFilter: FiltersParameters?,
                                               orderingPrice: Bool?) {
        productService?
            .getProductsByTypeFilters(isPopularProducts: isPopularProducts,
                                      nameFood: nil,
                                      categoryId: nil,
                                      filters: typeFilter,
                                      isDiscountProducts: nil,
                                      similarProduct: nil,
                                      orderingPrice: orderingPrice,
                                      storeId: "\(storeId)")
            .receive(on: RunLoop.main)
            .map(\.results)
            .compactMap({ products in
                return products.map({ ProductModel(productModel: $0) })
            })
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO: fetch filter Products успешно")
                    case .failure(let error):
                        self.state = .failure(error)
                    }
            }, receiveValue: { products in
                let removeDuplicatesArray = products.unique()
                self.state = .success(removeDuplicatesArray)
            }).store(in: &cancelable)
    }
}
