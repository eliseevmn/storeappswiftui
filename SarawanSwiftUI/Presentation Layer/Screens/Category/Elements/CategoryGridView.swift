//
//  CategoryGridView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 30.01.2022.
//

import SwiftUI

struct CategoryGridView: View {

    //MARK: - Properties
    var products: [ProductModel]
    @Binding var idProduct: Int?
    @Binding var isPresentedCardCell: Bool

    // MARK: - Bodt
    var body: some View {
        ScrollView(.vertical) {
            LazyVGrid(columns: [GridItem(), GridItem()], spacing: 10) {
                ForEach(products) { product in
                    ProductCellView(product: product,
                                    tapBuyButton: nil,
                                    isShowDiscount: true)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                        .onTapGesture {
                            self.idProduct = product.id
                            self.isPresentedCardCell.toggle()
                        }
                } // ForEach
            } // LazyVGrid
            .padding(.horizontal)
        } // ZStack
        .padding(.top, 95)
    } // Body
}

// MARK: - PreviewProvider
struct CategoryGridView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryGridView(products: [], idProduct: .constant(10), isPresentedCardCell: .constant(true))
    }
}
