//
//  ReturnCategoryView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 25.01.2022.
//

import SwiftUI

struct ReturnNavigationButton: View {

    // MARK: - Properties
    let title: String
    let dismissButton: (() -> Void)

    // MARK: - Body
    var body: some View {
        VStack(alignment: .center) {
            HStack(alignment: .center) {
                Button(action: dismissButton) {
                    Image(systemName: "arrow.left")
                        .foregroundColor(Color.blackApp)
                        .font(.montserratFont(.medium, size: 24))
                    Text(title)
                        .font(.montserratFont(.medium, size: 20))
                        .foregroundColor(Color.blackApp)
                        .lineLimit(2)
                }

                Spacer()
            }
        }

    } // Body
}

// MARK: - PreviewProvider
struct ReturnNavigationButton_Previews: PreviewProvider {
    static var previews: some View {
        ReturnNavigationButton(title: "Выгодные предложения", dismissButton: {

        })
    }
}
