//
//  DropDownMenuView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 24.01.2022.
//

import SwiftUI

struct DropDownMenuView: View {

    // MARK: - Properties
    @State var expend = false
    var filters = FilterType.allCases
    @Binding var defaultFilter: FilterType?
    var fetchProductsByFilter: ((FilterType) -> Void)

    // MARK: - Body
    var body: some View {
        VStack(alignment: .center) {
            HStack(alignment: .center) {
                Text(defaultFilter?.description ?? "").fontWeight(.heavy)
                    .font(.montserratFont(.regular, size: 16))
                Image(systemName: expend ? "chevron.down" : "chevron.up")
                    .resizable()
                    .frame(width: 10, height: 5)
            }
            .padding(10)
            .frame(maxWidth: .infinity)
            .foregroundColor(.blackApp)
            .background(Color.greenBackground)
            .onTapGesture {
                self.expend.toggle()
            }

            if expend {
                ForEach(filters, id: \.self) { filter in
                    Button(action: {
                        fetchProductsByFilter(filter)
                        defaultFilter = filter
                        self.expend.toggle()
                    }) {
                        Text(filter.description)
                            .font(.montserratFont(.regular, size: 16))
                            .padding(10)
                    }
                    .frame(maxWidth: .infinity)
                    .foregroundColor(.blackApp)
                    .background(defaultFilter == filter ? Color.greenBackground : Color.white)
                }
            }
        }
        .frame(width: 150)
        .background(expend ? Color.white : Color.greenBackground)
        .cornerRadius(20)
        .animation(.linear)
    }
}
