//
//  CategoryHeaderView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 30.01.2022.
//

import SwiftUI

struct CategoryHeaderView: View {

    // MARK: - Properties
    @Binding var foodName: String
    @Binding var filterType: FilterType?
    var onDismissButtonTapped: (() -> Void)
    var onChangeTypeFoodQuery: ((FilterType) -> Void)

    // MARK: - Body
    var body: some View {
        VStack(alignment: .center, spacing: 0) {

            HStack(alignment: .top, spacing: 0) {
                HStack {
                    ReturnNavigationButton(title: foodName,
                                           dismissButton: onDismissButtonTapped)
                        .padding(.leading, 20)

                    Spacer()
                }
                Spacer()

                DropDownMenuView(defaultFilter: $filterType,
                                 fetchProductsByFilter: { filter in
                    onChangeTypeFoodQuery(filter)
                })
                .padding(.trailing, 20)
            }
            Spacer()
        }
        .padding(.top, 45)
    }
}

//struct CategoryHeaderView_Previews: PreviewProvider {
//    static var previews: some View {
//        CategoryHeaderView()
//    }
//}
