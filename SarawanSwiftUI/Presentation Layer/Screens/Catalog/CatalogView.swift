//
//  CategoryView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 26.11.2021.
//

import SwiftUI

struct CatalogView: View {

    // MARK: - Properties
    @EnvironmentObject var catalogViewModel: CatalogViewModel
    @EnvironmentObject var categoryViewModel: CategoryViewModel
    @State var searchText: String = ""

    // MARK: - Body
    var body: some View {
        switch catalogViewModel.state {
        case .isLoading:
            ProgressView()
                .onAppear(perform: catalogViewModel.fetchCatalog)
        case .success(let catalog):
            successContent(catalog: catalog)
        case .failure(let error):
                Text(error.localizedDescription)
        }
    } // Body

    // MARK: - Helpers functions
    private func successContent(catalog: [CatalogModel]) -> some View {
        NavigationView {
            ScrollView(.vertical) {
                VStack(spacing: 0) {

                    LogoView()

                    SearchTextFieldView(
                        searchButtonTapped: { searchText in
                            self.searchText = searchText
                            self.categoryViewModel.state = .isLoading
                        },
                        destination: CategoryView(typeQuery: .productsBySearchName(foodName: self.searchText), foodName: $searchText)
                    )

                    CatalogTitleView()

                    CatalogListRow(searchText: $searchText, catalog: catalog) { catalogName in
                        self.searchText = catalogName
                        self.categoryViewModel.state = .isLoading
                    }
                } // VStack
                .padding(.top, 35)
            } // ScrollView
            .ignoresSafeArea()
            .navigationBarHidden(true)
            .padding(.bottom, 70)
        } //  NavigationView
    }
}

// MARK: - PreviewProvider
struct CatalogView_Previews: PreviewProvider {
    static var previews: some View {
        CatalogView()
    }
}

