//
//  CatalogTitleView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 24.01.2022.
//

import SwiftUI

struct CatalogTitleView: View {

    // MARK: - Body
    var body: some View {
        HStack {
            Text("Каталог")
                .font(.montserratFont(.medium, size: 32))
            Spacer()
        } // HStack
        .padding(.horizontal, 20)
        .padding(.vertical, 10)
    } // Body
}

// MARK: - PreviewProvider
struct CatalogTitleView_Previews: PreviewProvider {
    static var previews: some View {
        CatalogTitleView()
    }
}
