//
//  CatalogRowView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 24.01.2022.
//

import SwiftUI

struct CatalogRowView: View {

    // MARK: - Properties
    var catalogModel: CatalogModel

    // MARK: - Body
    var body: some View {
        VStack(spacing: 10) {
            HStack {
                Text(catalogModel.name ?? "Категория не найдена")
                    .font(.montserratFont(.regular, size: 16))
                    .foregroundColor(Color.blackApp)
                Spacer()
            } // HStack
            Divider()
                .frame(height: 1)
                .background(Color.lightGrayApp)
        } // VStack
        .padding(.horizontal, 20)
        .padding(.top, 10)
    } // Body
}

struct CatalogRowView_Previews: PreviewProvider {
    static var previews: some View {
        CatalogRowView(catalogModel: CatalogModel.init(categoryList: CatalogProductsListResponse.init(id: 111, name: "Овощи, фрукты", image: "1111", categories: [])))
    }
}
