//
//  CatalogListRow.swift
//  SarawanSwiftUI
//
//  Created by MAC on 30.01.2022.
//

import SwiftUI

struct CatalogListRow: View {

    // MARK: - Properties
    @Binding var searchText: String
    var catalog: [CatalogModel]
    var didEndTapGesture: ((String) -> Void)?

    // MARK: - Body
    var body: some View {
        VStack {
            ForEach(0..<catalog.count) { index in
                NavigationLink(destination: CategoryView(typeQuery: .productsPopular(catalogFoodId: catalog[index]), foodName: $searchText)) {
                    CatalogRowView(catalogModel: catalog[index])
                } // NavigationLink
                .simultaneousGesture(TapGesture().onEnded {
                    didEndTapGesture?(catalog[index].name ?? "")
                }) // NavigationLink
            } // ForEach
        } // VStack
    }
}

// MARK: - PreviewProvider
struct CatalogListRow_Previews: PreviewProvider {
    static var previews: some View {
        CatalogListRow(searchText: .constant("Яблоко"), catalog: [])
    }
}
