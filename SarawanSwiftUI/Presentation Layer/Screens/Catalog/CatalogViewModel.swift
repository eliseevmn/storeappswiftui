//
//  CatalogViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 09.01.2022.
//

import SwiftUI
import Combine

final class CatalogViewModel: ObservableObject {

    // MARK: - State
    enum State {
        case isLoading
        case success([CatalogModel])
        case failure(Error)
    }

    // MARK: - Properties
    @Published private(set) var state: State
    @Published var searchFoodText: String = ""
    private let productService: ProductServiceProtocol?
    private var cancelable = Set<AnyCancellable>()

    // MARK: - Init
    init(productService: ProductServiceProtocol?) {
        self.productService = productService
        self.state = .isLoading
    }

    // MARK: - Helpers functions
    func fetchCatalog() {
        self.productService?.getCategories()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO: fetchCatalog успешно")
                    case .failure(let error):
                        self.state = .failure(error)
                }
            }, receiveValue: { catalog in
                self.state = .success(catalog.map({ CatalogModel(categoryList: $0) }))
            })
            .store(in: &self.cancelable)
    }
}
