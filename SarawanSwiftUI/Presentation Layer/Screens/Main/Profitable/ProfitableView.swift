//
//  ProfitableView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import SwiftUI

struct ProfitableView: View {

    // MARK: - Properties
    @EnvironmentObject var mainViewModel: MainViewModel
    @EnvironmentObject var productViewModel: ProductCardViewModel
    @State private var isPresentedCardCell = false
    @State private var idProduct: Int? = 0

    // MARK: - Body
    var body: some View {
        switch mainViewModel.stateProfitableProducts {
            case .isLoading:
                createContent(products: ProductModel.listPlaceholders)
                    .onAppear(perform: mainViewModel.fetchsProfitableProducts)
                    .redacted(reason: .placeholder)
            case .success(let products):
                createContent(products: products)
            case .failure(let error):
                Text(error.localizedDescription)
        }
    } // Body

    // MARK: - Helpers functions
    private func createContent(products: [ProductModel]) -> some View {
        ZStack {
            VStack {
                TitleMainView(titleName: "Выгодные предложения")

                ProfitableListView(products: products,
                                   idProduct: $idProduct,
                                   isPresentedCardCell: $isPresentedCardCell)
            } // LazyVStack
            .padding(.trailing, 10)
            .padding(.leading, 20)
            .padding(.vertical, 20)
        } // ZStack
        .background(Color.favorableBackground)
        .fullScreenCover(isPresented: $isPresentedCardCell) {
            ProductCardView(idProduct: $idProduct)
                .onAppear {
                    productViewModel.stateProduct = .isLoading
                }
        }
    }
}
