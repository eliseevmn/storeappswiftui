//
//  ProfitableListView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 30.01.2022.
//

import SwiftUI

struct ProfitableListView: View {

    // MARK: - Properties
    var products: [ProductModel]
    @Binding var idProduct: Int?
    @Binding var isPresentedCardCell: Bool

    // MARK: - Body
    var body: some View {
        HStack(spacing: 5) {
            ForEach(products.prefix(2)) { product in
                ProductCellView(product: product, tapBuyButton: nil, isShowDiscount: true)
                    .onTapGesture {
                        self.idProduct = product.id
                        self.isPresentedCardCell.toggle()
                    }
            } // ForEach
        } // HStack
    } // Body
}

// MARK: - PreviewProvider
struct ProfitableListView_Previews: PreviewProvider {
    static var previews: some View {
        ProfitableListView(products: [], idProduct: .constant(10), isPresentedCardCell: .constant(true))
    }
}
