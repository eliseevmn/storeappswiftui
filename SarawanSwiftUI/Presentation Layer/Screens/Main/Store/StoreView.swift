//
//  StoreView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct StoresView: View {

    // MARK: - Properties
    @EnvironmentObject var mainViewModel: MainViewModel
    @EnvironmentObject var categoryViewModel: CategoryViewModel

    // MARK: - Body
    var body: some View {
        switch mainViewModel.stateStoresProducts {
            case .isLoading:
                createContent(stores: StoreModel.listPlaceholder)
                    .onAppear(perform: mainViewModel.fetchStores)
                    .redacted(reason: .placeholder)
            case .success(let stores):
                createContent(stores: stores)
            case .failure(let error):
                Text(error.localizedDescription)
        }
    } // Body

    // MARK: - Helpers functions
    private func createContent(stores: [StoreModel]) -> some View {
        VStack {
            TitleMainView(titleName: "Наши магазины")
            StoreListView(stores: stores) {
                self.categoryViewModel.state = .isLoading
            }
        } // VStack
        .background(Color.whiteApp)
        .padding(.trailing, 10)
        .padding(.leading, 20)
        .padding(.bottom, 100)
    }
}
