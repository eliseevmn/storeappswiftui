//
//  StoreListView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 30.01.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct StoreListView: View {

    // MARK: - Properties
    var stores: [StoreModel]
    @State var storeName: String = ""
    var didEndTapGesture: (() -> Void)?

    // MARK: - Body
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(stores) { store in
                    NavigationLink(destination: CategoryView(typeQuery: .productsByStore(storeModel: store),
                                                             foodName: $storeName)) {
                        HStack {
                            WebImage(url: URL(string: store.logo ?? ""))
                                .resizable()
                                .placeholder(Image("icDefaultFood"))
                                .aspectRatio(1, contentMode: .fit)
                                .frame(width: 40, height: 40)
                            Text(store.name ?? "Неизвестное")
                        } // HStack
                    } // NavigationLink
                    .simultaneousGesture(TapGesture().onEnded {
                        self.storeName = store.name ?? ""
                        didEndTapGesture?()
                    }) // NavigationLink
                } // ForEach
            } //HStack
        } // ScrollView
    } // Body
}

// PreviewProvider
struct StoreListView_Previews: PreviewProvider {
    static var previews: some View {
        StoreListView(stores: [])
    }
}
