//
//  MainView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import SwiftUI

struct MainView: View {

    @State private var searchText: String = ""
    @EnvironmentObject var categoryViewModel: CategoryViewModel

    // MARK: - Properties
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 0) {
                    LogoView()
                    SearchTextFieldView(
                        searchButtonTapped: { searchText in
                            self.searchText = searchText
                            self.categoryViewModel.state = .isLoading
                        },
                        destination: CategoryView(typeQuery: .productsBySearchName(foodName: searchText), foodName: $searchText)
                    )
                    ProfitableView()
                    RecommentedView()
                    StoresView()
                } // VStack
            } // ScrollView
            .navigationBarHidden(true)
        } // NavigationView
    } // Body
}

// MARK: - PreviewProvider
struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
