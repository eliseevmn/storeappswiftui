//
//  RecommentedView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import SwiftUI
import UIKit

struct RecommentedView: View {

    // MARK: - Properties
    @EnvironmentObject var mainViewModel: MainViewModel
    @EnvironmentObject var productViewModel: ProductCardViewModel
    @State private var isPresentedCardCell = false
    @State private var idProduct: Int? = 0

    // MARK: - Body
    var body: some View {
        switch mainViewModel.stateRecommentedProducts {
            case .isLoading:
                createContent(products: ProductModel.listRecommentedPlaceholders)
                    .onAppear(perform: mainViewModel.fetchRecommentedProducts)
                    .redacted(reason: .placeholder)
            case .success(let products):
                createContent(products: products)
            case .failure(let error):
                Text(error.localizedDescription)
        }
    } // Body

    // MARK: - Helpers functions
    private func createContent(products: [ProductModel]) -> some View {
        ZStack {
            VStack {
                TitleMainView(titleName: "Мы рекомендуем")

                RecommentedListView(products: products,
                                    idProduct: $idProduct,
                                    isPresentedCardCell: $isPresentedCardCell)
            } // VStack
            .padding(.trailing, 20)
            .padding(.leading, 20)
            .padding(.vertical, 20)
        } // ZStack
        .background(Color.whiteApp)
        .fullScreenCover(isPresented: $isPresentedCardCell) {
            ProductCardView(idProduct: $idProduct)
                .onAppear {
                    productViewModel.stateProduct = .isLoading
                }
        }
    }
}

// MARK: - PreviewProvider
struct RecommentedView_Previews: PreviewProvider {
    static var previews: some View {
        RecommentedView()
    }
}
