//
//  RecommentedListView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 30.01.2022.
//

import SwiftUI

struct RecommentedListView: View {

    // MARK: - Properties
    var products: [ProductModel]
    @Binding var idProduct: Int?
    @Binding var isPresentedCardCell: Bool

    // MARK: - Body
    var body: some View {
        LazyVGrid(columns: [GridItem(), GridItem()], spacing: 10) {
            ForEach(products.prefix(10), id: \.self) { product in
                ProductCellView(product: product, tapBuyButton: nil, isShowDiscount: false)
                    .padding(.bottom, 10)
                    .onTapGesture {
                        self.idProduct = product.id
                        self.isPresentedCardCell.toggle()
                    }
            } // ForEach
        } // LazyVGrid
    } // Body
}

struct RecommentedListView_Previews: PreviewProvider {
    static var previews: some View {
        RecommentedListView(products: [], idProduct: .constant(10), isPresentedCardCell: .constant(true))
    }
}
