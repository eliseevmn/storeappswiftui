//
//  MainViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 05.01.2022.
//

import SwiftUI
import Combine

final class MainViewModel: ObservableObject {

    // MARK: - State
    enum State<T>{
        case isLoading
        case success(T)
        case failure(Error)
    }

    // MARK: - Properties
    @Published private(set) var stateProfitableProducts: State<[ProductModel]>
    @Published private(set) var stateRecommentedProducts: State<[ProductModel]>
    @Published private(set) var stateStoresProducts: State<[StoreModel]>
    private let productService: ProductServiceProtocol?
    private let storeService: StoreServiceProtocol?
    private var cancelable = Set<AnyCancellable>()

    // MARK: - Init
    init(productService: ProductServiceProtocol?,
         storeService: StoreServiceProtocol?) {
        self.productService = productService
        self.storeService = storeService
        self.stateProfitableProducts = .isLoading
        self.stateRecommentedProducts = .isLoading
        self.stateStoresProducts = .isLoading
    }

    // MARK: - Helpers functions
    func fetchRecommentedProducts() {
        productService?
            .getProductsByTypeFilters(isPopularProducts: nil,
                                      nameFood: nil,
                                      categoryId: nil,
                                      filters: nil,
                                      isDiscountProducts: nil,
                                      similarProduct: nil,
                                      orderingPrice: false,
                                      storeId: nil)
            .map(\.results)
            .map({ $0.shuffled()})
            .receive(on: RunLoop.main)
            .compactMap({ products in
                return products.map({ ProductModel(productModel: $0) })
            })
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO: fetchRecommentedProducts успешно")
                    case .failure(let error):
                        self.stateRecommentedProducts = .failure(error)
                }
            }, receiveValue: { products in
                let removeDuplicatesArray = products.unique()
                self.stateRecommentedProducts = .success(removeDuplicatesArray)
            })
            .store(in: &self.cancelable)
    }

    func fetchsProfitableProducts() {
        productService?
            .getProductsByTypeFilters(isPopularProducts: nil,
                                      nameFood: nil,
                                      categoryId: nil,
                                      filters: nil,
                                      isDiscountProducts: "true",
                                      similarProduct: nil,
                                      orderingPrice: false,
                                      storeId: nil)
            .map(\.results)
            .map({ $0.shuffled()})
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO fetchsProfitableProducts успешно")
                    case .failure(let error):
                        self.stateProfitableProducts = .failure(error)
                }
            }, receiveValue: { products in
                self.stateProfitableProducts = .success(products.map({ ProductModel(productModel: $0) }))
            })
            .store(in: &self.cancelable)
    }

    func fetchStores() {
        self.storeService?.getAllStores()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .finished:
                        print("TODO fetchStores успешно")
                    case .failure(let error):
                        self.stateStoresProducts = .failure(error)
                }
            }, receiveValue: { catalog in
                self.stateStoresProducts = .success(catalog.map({ StoreModel(storesInfo: $0) }))
            })
            .store(in: &self.cancelable)
    }
}
