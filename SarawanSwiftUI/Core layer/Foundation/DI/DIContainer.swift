//
//  DIContainer.swift
//  SarawanSwiftUI
//
//  Created by MAC on 09.01.2022.
//

import Foundation

final class DIContainer {

    // MARK: - Public Properties

    let session: URLSession
    let decoder: JSONDecoder

    let keychainWrapper: UserDefaults
    let requestBuilder: RequestBuilder
    let apiClient: APIClient
    let tokenRepository: TokenRepository

    lazy var userService = UserService(userAPIClient: apiClient, tokenRepository: tokenRepository)
    lazy var authService = AuthService(authAPIClient: apiClient, tokenRepository: tokenRepository)
    lazy var productService = ProductService(productAPIClient: apiClient)
    lazy var storesService = StoreService(storeAPIClient: apiClient)

    // MARK: - Initialisers

    init() {
        keychainWrapper = UserDefaults.standard
        tokenRepository = TokenRepository(keychainWrapper: keychainWrapper)
        requestBuilder = RequestBuilder()
        session = URLSession.shared
        decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        apiClient = APIClient(requestBuilder: requestBuilder, session: session, decoder: decoder)
    }
}
