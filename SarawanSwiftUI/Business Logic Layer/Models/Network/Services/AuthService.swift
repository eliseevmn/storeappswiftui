//
//  AuthService.swift
//  SarawanSwiftUI
//
//  Created by MAC on 16.01.2022.
//

import Foundation
import Combine

protocol AuthServiceProtocol {
    func sendSms(to phoneNumber: String) -> AnyPublisher<String, Error>
    func getToken(for phoneNumber: String, code: String) -> AnyPublisher<APIClient.TokenResponse, Error>
}

final class AuthService: AuthServiceProtocol {

    // MARK: - Private Properties
    private let authAPIClient: AuthAPIClient
    private let tokenRepository: TokenStorable

    // MARK: - Initialisers
    init(authAPIClient: AuthAPIClient, tokenRepository: TokenStorable) {
        self.authAPIClient = authAPIClient
        self.tokenRepository = tokenRepository
    }

    // MARK: - Public Methods
    func sendSms(to phoneNumber: String) -> AnyPublisher<String, Error> {
        return authAPIClient.sms(to: phoneNumber)
            .map { result in String(result.success) }
            .eraseToAnyPublisher()
    }

    func getToken(for phoneNumber: String, code: String) -> AnyPublisher<APIClient.TokenResponse, Error> {
        return authAPIClient.token(for: phoneNumber, code: code)
            .map { [weak tokenRepository] tokenResponse in
                tokenRepository?.token = tokenResponse.token
                tokenRepository?.userId = tokenResponse.userId
                return tokenResponse
            }
            .eraseToAnyPublisher()
    }
}
