//
//  InternetMonitor.swift
//  SarawanSwiftUI
//
//  Created by MAC on 25.11.2021.
//

import Foundation
import Network

class InternetMonitor: ObservableObject {

    // MARK: - Properties
    @Published var isAvailable = true
    private let monitor = NWPathMonitor()

    // MARK: - Init
    init() {
        internetMonitoring()
    }

    // MARK: - Monitor functions
    private func internetMonitoring() {
        let queue = DispatchQueue(label: "Sarawan.InternetMonitor")
        monitor.pathUpdateHandler = { path in
            DispatchQueue.main.async {
                self.isAvailable = path.status == .satisfied
            }
        }
        monitor.start(queue: queue)
    }
}
