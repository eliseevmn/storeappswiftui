//
//  StoreService.swift
//  SarawanSwiftUI
//
//  Created by MAC on 09.01.2022.
//

import Foundation
import Combine

protocol StoreServiceProtocol {
    func getAllStores() -> AnyPublisher<[StoresInfoResponse], Error>
}

final class StoreService: StoreServiceProtocol {

    // MARK: - Properties
    private let storeAPIClient: StoreAPIClient

    // MARK: - Init
    init(storeAPIClient: StoreAPIClient) {
        self.storeAPIClient = storeAPIClient
    }

    // MARK: - Public Methods
    func getAllStores() -> AnyPublisher<[StoresInfoResponse], Error> {
        return storeAPIClient
            .getAllStores()
            .eraseToAnyPublisher()
    }
}
