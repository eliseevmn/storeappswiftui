//
//  UserService.swift
//  SarawanSwiftUI
//
//  Created by MAC on 16.01.2022.
//

import Foundation
import Combine

// Класс, который делает промежуточные расчеты, последовательные запросы нескольких API методов.
// В этом классе формируются готовые методы которые будут вызваны из ViewModel
// Думаю здесь делать всю необходимую фильтрацию, валидацию, map'ы чтобы VM получала уже чистые данные для отображения.

// swiftlint:disable identifier_name
protocol UserServiceProtocol {
    var userId: Int { get }

    func getUser() -> AnyPublisher<UserModel, Error>
    func updateUser(with data: [String: String]) -> AnyPublisher<UserModel, Error>

    func addresses() -> AnyPublisher<[AddressModel], Error>
    func address(by id: Int) -> AnyPublisher<AddressModel, Error>
    func newAddress(with data: AddressModel)  -> AnyPublisher<AddressModel, Error>
    func updateAddress(by id: Int, with data: AddressModel) -> AnyPublisher<AddressModel, Error>
}

final class UserService: UserServiceProtocol {

    // MARK: - Public Properties

    var userId: Int {
        guard let userId = tokenRepository.userId else { return 0 }
        return userId
    }

    // MARK: - Private Properties

    private let userAPIClient: UserAPIClient
    private let tokenRepository: TokenStorable

    // MARK: - Initialisers

    init(userAPIClient: UserAPIClient, tokenRepository: TokenStorable) {
        self.userAPIClient = userAPIClient
        self.tokenRepository = tokenRepository
    }

    // MARK: - Public Methods

    func getUser() -> AnyPublisher<UserModel, Error> {
        return userAPIClient.user(by: tokenRepository.userId ?? 0, token: tokenRepository.token ?? "")
            .eraseToAnyPublisher()
    }

    func updateUser(with data: [String: String]) -> AnyPublisher<UserModel, Error> {
        return userAPIClient.updateUser(by: tokenRepository.userId ?? 0, with: data, token: tokenRepository.token ?? "")
            .eraseToAnyPublisher()
    }

    func addresses() -> AnyPublisher<[AddressModel], Error> {
        return userAPIClient.addresses(token: tokenRepository.token ?? "")
    }

    func address(by id: Int) -> AnyPublisher<AddressModel, Error> {
        return userAPIClient.address(by: id, token: tokenRepository.token ?? "")
    }

    func newAddress(with data: AddressModel) -> AnyPublisher<AddressModel, Error> {
        return userAPIClient.newAddress(with: data, token: tokenRepository.token ?? "")
    }

    func updateAddress(by id: Int, with data: AddressModel) -> AnyPublisher<AddressModel, Error> {
        return userAPIClient.updateAddress(by: id, with: data, token: tokenRepository.token ?? "")
    }

}
