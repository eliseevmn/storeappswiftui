//
//  ProductService.swift
//  Sarawan
//
//  Created by MacBook Pro on 29.11.2021.
//

import Foundation
import Combine


protocol ProductServiceProtocol {
    func getProduct(id: Int) -> AnyPublisher<ProductsCommonInfoResponse.ProductModelResponse, Error>
    func getCategories() -> AnyPublisher<[CatalogProductsListResponse], Error>
    func getProductsByTypeFilters(isPopularProducts: String?,
                                  nameFood: String?,
                                  categoryId: String?,
                                  filters: FiltersParameters?,
                                  isDiscountProducts: String?,
                                  similarProduct: Int?,
                                  orderingPrice: Bool?,
                                  storeId: String?) -> AnyPublisher<ProductsCommonInfoResponse, Error>
}

final class ProductService: ProductServiceProtocol {

	// MARK: - Private Properties
	private let productAPIClient: ProductAPIClient

	// MARK: - Initialisers
	init(productAPIClient: ProductAPIClient) {
		self.productAPIClient = productAPIClient
	}

	// MARK: - Public Methods
    func getCategories() -> AnyPublisher<[CatalogProductsListResponse], Error> {
        return productAPIClient
            .getAllCategories()
            .eraseToAnyPublisher()
    }

    func getProductsByTypeFilters(isPopularProducts: String?,
                                  nameFood: String?,
                                  categoryId: String?,
                                  filters: FiltersParameters?,
                                  isDiscountProducts: String?,
                                  similarProduct: Int?,
                                  orderingPrice: Bool?,
                                  storeId: String?) -> AnyPublisher<ProductsCommonInfoResponse, Error> {
        return productAPIClient
            .getProductsByTypeFilters(isPopularProducts: isPopularProducts,
                                      nameFood: nameFood,
                                      categoryId: categoryId,
                                      filters: filters,
                                      isDiscountProducts: isDiscountProducts,
                                      similarProduct: similarProduct,
                                      orderingPrice: orderingPrice,
                                      storeId: storeId)
            .eraseToAnyPublisher()
    }

    func getProduct(id: Int) -> AnyPublisher<ProductsCommonInfoResponse.ProductModelResponse, Error> {
        return productAPIClient
            .getProductModel(id: id)
            .eraseToAnyPublisher()

    }
}
