//
//  ProductsCommon.swift
//  SarawanSwiftUI
//
//  Created by MAC on 25.11.2021.
//

import Foundation

struct ProductsCommonInfoResponse: Codable {

    // MARK: - Properties
    let count: Int?
    let next: String?
    let previous: String?
    let results: [ProductModelResponse]
    

    struct ProductModelResponse: Codable {
        let idProduct: Int?
        let name: String?
        let priceType: String?
        let unitQuantity: String?
        let description: String?
        let information: Information
        let images: [ImageResponse]
        let category: String?
        let mainCategory: String?
        let storePrices: [StorePrice]
        let isFavorite: Bool?

        enum CodingKeys: String, CodingKey {
            case idProduct = "id"
            case name
            case priceType = "price_type"
            case unitQuantity = "unit_quantity"
            case description
            case information
            case images
            case category
            case mainCategory = "main_category"
            case storePrices
            case isFavorite = "is_favorite"
        }
    }

    struct Information: Codable {
        let productNutrition: String?
        let productInformation: String?
        let productIngredients: String?

        enum CodingKeys: String, CodingKey {
            case productNutrition = "product_nutrition"
            case productInformation = "product_information"
            case productIngredients = "product_ingredients"
        }
    }

    struct ImageResponse: Codable {
        let idImage: Int?
        let imageUrl: String?

        enum CodingKeys: String, CodingKey {
            case idImage = "id"
            case imageUrl = "image"
        }
    }

    struct StorePrice: Codable {
        let idProduct: Int?
        let storeName: String?
        let priceFood: String?
        let productId: Int?
        let discount: Int?

        enum CodingKeys: String, CodingKey {
            case idProduct = "id"
            case storeName = "store"
            case priceFood = "price"
            case productId = "product"
            case discount
        }
    }
}
