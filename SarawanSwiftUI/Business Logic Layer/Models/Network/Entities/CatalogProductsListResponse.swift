//
//  CategoriesListResponse.swift
//  SarawanSwiftUI
//
//  Created by MAC on 26.11.2021.
//

import Foundation

struct CatalogProductsListResponse: Codable, Identifiable {

    // MARK: - Properties
    let id: Int?
    let name: String?
    let image: String?
    let categories: [CategoryModelResponse]

    struct CategoryModelResponse: Codable {
        let id: Int?
        let name: String?
    }
}
