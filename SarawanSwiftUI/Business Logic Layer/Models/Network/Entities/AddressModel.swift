//
//  Address.swift
//  SarawanSwiftUI
//
//  Created by MAC on 16.02.2022.
//

import Foundation

struct AddressModel: Codable {
    var addressId: Int
    var city: String
    var street: String
    var house: String
    var building: String
    var housing: String
    var roomNumber: String
    var primary: Bool
    var user: Int

    enum CodingKeys: String, CodingKey {
        case addressId = "id"
        case city
        case street
        case house
        case building
        case housing
        case roomNumber
        case primary
        case user
    }

    var fullAddress: String {
        if isEmptyAdddress { return "Добавить адресс"}
        if !housing.isEmpty, !roomNumber.isEmpty {
            return "г. \(city), ул. \(street), д. \(house), подъезд \(housing), кв. \(roomNumber )"
        }
        if !housing.isEmpty {
            return "г. \(city), ул. \(street), д. \(house), кв. \(roomNumber )"
        }
        return "г. \(city), ул. \(street), д. \(house), кв. \(roomNumber)"
    }

    var isEmptyAdddress: Bool {
        city.isEmpty || street.isEmpty || house.isEmpty || roomNumber.isEmpty
    }

}
