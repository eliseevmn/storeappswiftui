//
//  StoresInfoResponse.swift
//  SarawanSwiftUI
//
//  Created by MAC on 26.11.2021.
//

import Foundation

struct StoresInfoResponse: Codable {

    // MARK: - Properties
    let id: Int?
    let name: String?
    let logo: String?
}
