//
//  UserResponse.swift
//  SarawanSwiftUI
//
//  Created by MAC on 13.01.2022.
//

import Foundation

struct UserModel: Codable {
    var firstName: String
    let lastName: String
    let email: String
    let image: String?
    let birthday: String?
    let phone: String
    let status: String

    enum CodingKeys: String, CodingKey {
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case image
        case birthday
        case phone
        case status
    }

    var fullName: String {
        if isEmptyName { return "Добавить имя"}
        return "\(firstName) \(lastName)"
    }

    var isEmptyName: Bool {
        firstName.isEmpty || lastName.isEmpty
    }
}
