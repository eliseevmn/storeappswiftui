//
//  StoreAPIClient.swift
//  SarawanSwiftUI
//
//  Created by MAC on 09.01.2022.
//

import Foundation
import Combine

protocol StoreAPIClient {
    func getAllStores() -> AnyPublisher<[StoresInfoResponse], Error>
}

extension APIClient: StoreAPIClient {

    func getAllStores() -> AnyPublisher<[StoresInfoResponse], Error> {
        let request = requestBuilder
            .set(path: .getStores)
            .set(method: .GET)
            .build()
        return performRequest(request)
    }
}
