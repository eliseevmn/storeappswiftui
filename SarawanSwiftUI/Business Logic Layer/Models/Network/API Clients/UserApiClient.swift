//
//  UserApiClient.swift
//  SarawanSwiftUI
//
//  Created by MAC on 16.01.2022.
//

import Foundation
import Combine

// Класс, который содержит все API методы для юзера. Здесь идет настройка запроса через RequestBuilder.
// Возвращает Паблишер, который содержит в себе запрашиваемые данные. Где-то это готовые entity, где-то response как в products.
// Фактического запроса не происходит. Он будет только когда появится подписчик. Все подписки предлагаю делать во VM.

// swiftlint:disable identifier_name
protocol UserAPIClient {
    // func userList() -> AnyPublisher<[UserModel], Error>
    func user(by id: Int, token: String) -> AnyPublisher<UserModel, Error>
    func updateUser(by id: Int, with data: [String: String], token: String) -> AnyPublisher<UserModel, Error>

    func addresses(token: String) -> AnyPublisher<[AddressModel], Error>
    func address(by id: Int, token: String) -> AnyPublisher<AddressModel, Error>
    func newAddress(with data: AddressModel, token: String)  -> AnyPublisher<AddressModel, Error>
    func updateAddress(by id: Int, with data: AddressModel, token: String) -> AnyPublisher<AddressModel, Error>
}

extension APIClient: UserAPIClient {

    func user(by id: Int, token: String) -> AnyPublisher<UserModel, Error> {
        let request = requestBuilder
            .set(path: .user(id: id))
            .set(method: .GET)
            .add(header: "Authorization", value: "Token \(token)")
            .build()

        return performRequest(request)
    }

    func updateUser(by id: Int, with data: [String: String], token: String) -> AnyPublisher<UserModel, Error> {
        let request = requestBuilder
            .set(path: .user(id: id))
            .set(method: .PUT)
            .set(parameters: data)
            .add(header: "Authorization", value: "Token \(token)")
            .build()

        return performRequest(request)
    }

    func addresses(token: String) -> AnyPublisher<[AddressModel], Error> {
        let request = requestBuilder
            .set(path: .addresses)
            .set(method: .GET)
            .add(header: "Authorization", value: "Token \(token)")
            .build()

        return performRequest(request)
    }

    func address(by id: Int, token: String) -> AnyPublisher<AddressModel, Error> {
        let request = requestBuilder
            .set(path: .address(id: id))
            .set(method: .GET)
            .add(header: "Authorization", value: "Token \(token)")
            .build()

        return performRequest(request)
    }

    func newAddress(with data: AddressModel, token: String) -> AnyPublisher<AddressModel, Error> {
        let request = requestBuilder
            .set(path: .addresses)
            .set(method: .POST)
            .set(parameters: data)
            .add(header: "Authorization", value: "Token \(token)")
            .build()

        return performRequest(request)
    }

    func updateAddress(by id: Int, with data: AddressModel, token: String) -> AnyPublisher<AddressModel, Error> {
        let request = requestBuilder
            .set(path: .address(id: id))
            .set(method: .PUT)
            .set(parameters: data)
            .add(header: "Authorization", value: "Token \(token)")
            .build()

        return performRequest(request)
    }
}
