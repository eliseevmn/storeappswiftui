//
//  AuthAPIClient.swift
//  SarawanSwiftUI
//
//  Created by MAC on 16.01.2022.
//

import Foundation
import Combine

protocol AuthAPIClient {
    func sms(to phoneNumber: String) -> AnyPublisher<APIClient.SmsResponse, Error>
    func token(for phoneNumber: String, code: String) -> AnyPublisher<APIClient.TokenResponse, Error>
}

extension APIClient: AuthAPIClient {
    func sms(to phoneNumber: String) -> AnyPublisher<APIClient.SmsResponse, Error> {
        let request = requestBuilder
            .set(path: .sms)
            .set(method: .POST)
            .set(parameters: ["phone_number": phoneNumber])
            .build()

        return performRequest(request)
    }

    func token(for phoneNumber: String, code: String) -> AnyPublisher<APIClient.TokenResponse, Error> {
        let request = requestBuilder
            .set(path: .token)
            .set(method: .POST)
            .set(parameters: ["phone_number": phoneNumber,
                              "code": code
            ])
            .build()

        return performRequest(request)
    }

}

extension APIClient {
    struct SmsResponse: Decodable {
        let success: Bool
    }

    struct TokenResponse: Decodable {
        let token: String
        let userId: Int
    }
}
