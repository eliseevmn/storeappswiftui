//
//  APIClient.swift
//  Sarawan
//
//  Created by MacBook Pro on 26.11.2021.
//

import Foundation
import Combine

protocol ProductAPIClient {
    func getProductModel(id: Int) -> AnyPublisher<ProductsCommonInfoResponse.ProductModelResponse, Error>
    func getAllCategories() -> AnyPublisher<[CatalogProductsListResponse], Error>
    func getProductsByTypeFilters(isPopularProducts: String?,
                                  nameFood: String?,
                                  categoryId: String?,
                                  filters: FiltersParameters?,
                                  isDiscountProducts: String?,
                                  similarProduct: Int?,
                                  orderingPrice: Bool?,
                                  storeId: String?) -> AnyPublisher<ProductsCommonInfoResponse, Error>
}

extension APIClient: ProductAPIClient {

    func getAllCategories() -> AnyPublisher<[CatalogProductsListResponse], Error> {
        let request = requestBuilder
            .set(path: .categories)
            .set(method: .GET)
            .build()
        return performRequest(request)
    }

    func getProductsByTypeFilters(isPopularProducts: String?,
                                  nameFood: String?,
                                  categoryId: String?,
                                  filters: FiltersParameters?,
                                  isDiscountProducts: String?,
                                  similarProduct: Int?,
                                  orderingPrice: Bool?,
                                  storeId: String?) -> AnyPublisher<ProductsCommonInfoResponse, Error> {
        let request = requestBuilder
            .set(path: .getProducts)
            .set(method: .GET)
            .set(parameters: ["popular_products": "\(isPopularProducts ?? "")",
                              "discount_products": "\(isDiscountProducts ?? "")",
                              "product_name": nameFood ?? "",
                              "main_category": categoryId ?? "",
                              "order": "\(filters?.description ?? "")",
                              "similarProduct": "\(similarProduct ?? 0)",
                              "ordering_price": String(orderingPrice ?? false),
                              "store": storeId ?? ""
                             ])
            .build()
        return performRequest(request)
    }

    func getProductModel(id: Int) -> AnyPublisher<ProductsCommonInfoResponse.ProductModelResponse, Error> {
        let request = requestBuilder
            .set(path: .getProduct(id: id))
            .set(method: .GET)
            .set(parameters: ["":""])
            .build()
        return performRequest(request)
    }
}
