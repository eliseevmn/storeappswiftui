//
//  FilterType.swift
//  SarawanSwiftUI
//
//  Created by MAC on 25.01.2022.
//

import Foundation
import SwiftUI

enum FilterType: CaseIterable {
    case upPriceFilter
    case downPriceFilter
    case alphabetFilter
    case upDiscountFilter
    case downDiscountFilter
    case popularFilter

    var description: String {
        switch self {
        case .upPriceFilter:
            return "Цене"
        case .downPriceFilter:
            return "Цене"
        case .alphabetFilter:
            return "Алфавиту"
        case .upDiscountFilter:
            return "Скидке"
        case .downDiscountFilter:
            return "Скидке"
        case .popularFilter:
            return "Популярные"
        }
    }

    var image: Image {
        switch self {
        case .upPriceFilter:
            return Image("icArrowUp")
        case .downPriceFilter:
            return Image("icArrowDown")
        case .alphabetFilter:
            return Image("")
        case .upDiscountFilter:
            return Image("icArrowUp")
        case .downDiscountFilter:
            return Image("icArrowDown")
        case .popularFilter:
            return Image("")
        }
    }
}
