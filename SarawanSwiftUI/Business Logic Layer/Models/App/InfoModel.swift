//
//  InfoModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 05.01.2022.
//

import Foundation

class InfoModel: Hashable {

    let icon: String
    let title: String

    init(icon: String, title: String) {
        self.icon = icon
        self.title = title
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
    }

    static func == (lhs: InfoModel, rhs: InfoModel) -> Bool {
        return lhs.title == rhs.title
    }
}
