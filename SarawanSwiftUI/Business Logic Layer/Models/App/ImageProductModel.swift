//
//  ImageProductModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.02.2022.
//

import Foundation

struct ImageProductModel: Identifiable {
    let id: String
    let imageURL: String

    init(imageResponse: ProductsCommonInfoResponse.ImageResponse) {
        self.id = "\(imageResponse.idImage ?? 0)"
        self.imageURL = imageResponse.imageUrl ?? ""
    }
}
