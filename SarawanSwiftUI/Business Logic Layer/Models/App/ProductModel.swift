//
//  ProductsViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 25.11.2021.
//

import Foundation

struct ProductModel: Equatable, Identifiable, Hashable {

    // MARK: - Properties
    let id: Int
    let nameFood: String
    let imageFood: String
    let imagesFood: [ImageProductModel]
    let storesFood: [StorePriceModel]
    let priceFood: String
    let priceType: String
    let unitQuantity: String
    var discountFood: String
    let storeName: String
    let descriptionProduct: String

    // MARK: - Init
    init(productModel: ProductsCommonInfoResponse.ProductModelResponse) {
        self.id = productModel.idProduct ?? 0
        self.nameFood = productModel.name ?? ""
        self.imageFood = productModel.images.first?.imageUrl ?? "testImage"
        self.priceFood = productModel.storePrices.first?.priceFood ?? "0"
        self.priceType = productModel.priceType ?? ""
        self.unitQuantity = productModel.unitQuantity ?? ""
        self.storeName = productModel.storePrices.first?.storeName ?? ""
        self.discountFood = "-\(productModel.storePrices.first?.discount ?? 0)"
        self.descriptionProduct = productModel.description ?? "Описание отсутствует"
        self.imagesFood = productModel.images.map({ ImageProductModel(imageResponse: $0) })
        self.storesFood = productModel.storePrices.map({ StorePriceModel(storePrice: $0) })
    }

    // MARK: - Placeholders
    static let placeholder = createPlaceholder(id: 1)
    static var listPlaceholders: [ProductModel] {
        (0..<2).map({
            createPlaceholder(id: $0)
        })
    }
    static var listRecommentedPlaceholders: [ProductModel] {
        (0..<10).map({
            createPlaceholder(id: $0)
        })
    }

    private static func createPlaceholder(id: Int) -> ProductModel {
        return ProductModel(productModel: .init(idProduct: id,
                                                name: "Cладкий подарочный набор Атаг Ищи момент с фундуком 200 г",
                                                priceType: "Шт",
                                                unitQuantity: "0.2",
                                                description: "Набор из конфет продолговатой формы в темной кондитерской глазури с нежной начинкой с добавлением дробленого фундука и дробленого арахиса, со вкусами трюфеля, шоколадно-орехового крема, фундука.",
                                                information: .init(productNutrition: "", productInformation: "", productIngredients: ""),
                                                images: [.init(idImage: 348192, imageUrl: "https://dev.sarawan.ru/media/0222dff40313ec7a3d7fcc17e8fc4b7e726b6e38.jpg")],
                                                category: "Конфеты",
                                                mainCategory: "Сладости, снеки",
                                                storePrices: [.init(idProduct: 395638, storeName: "Глобус", priceFood: "409.99", productId: 334877, discount: 10),
                                                              .init(idProduct: 395638, storeName: "Ашан", priceFood: "209.99", productId: 334877, discount: 10),
                                                              .init(idProduct: 395638, storeName: "Пяторочка", priceFood: "109.99", productId: 334877,discount: 10)],
                                                isFavorite: false))
    }

    // MARK: - Helpers functions
    static func == (lhs: ProductModel, rhs: ProductModel) -> Bool {
        return lhs.id == rhs.id
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
