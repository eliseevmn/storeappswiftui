//
//  StorePriceModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.02.2022.
//

import Foundation

struct StorePriceModel: Identifiable {

    let id: String
    let name: String
    let price: String
    let productId: String
    let discount: String

    init(storePrice: ProductsCommonInfoResponse.StorePrice) {
        self.id = "\(storePrice.idProduct ?? 0)"
        self.name = storePrice.storeName ?? ""
        self.price = storePrice.priceFood ?? ""
        self.productId = "\(storePrice.productId ?? 0)"
        self.discount = "\(storePrice.discount ?? 0)"
    }
}
