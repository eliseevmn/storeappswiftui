//
//  FormValidation.swift
//  SarawanSwiftUI
//
//  Created by MAC on 18.01.2022.
//

import Foundation

struct FormValidation {
    var success: Bool = false
    var message: String = ""
}
