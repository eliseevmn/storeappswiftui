//
//  CategoriesListViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 26.11.2021.
//

import Foundation

struct CatalogModel: Equatable, Identifiable {

    // MARK: - Properties
    let id: Int?
    let name: String?
    let image: String?
    let categories: [CategoryModel]

    // MARK: - Init
    init(categoryList: CatalogProductsListResponse) {
        self.id = categoryList.id
        self.name = categoryList.name
        self.image = categoryList.image
        self.categories = categoryList.categories.map { CategoryModel(categoryModel: $0) }
    }
}
