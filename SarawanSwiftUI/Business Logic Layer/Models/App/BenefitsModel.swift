//
//  BenefitsModel.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import Foundation

final class BenefitModel: Hashable {

    let icon: String
    let title: String
    let description: String

    init(icon: String, title: String, description: String) {
        self.icon = icon
        self.title = title
        self.description = description
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(title)
    }

    static func == (lhs: BenefitModel, rhs: BenefitModel) -> Bool {
        return lhs.title == rhs.title
    }
}
