//
//  TypeFoodQuery.swift
//  SarawanSwiftUI
//
//  Created by MAC on 26.01.2022.
//

import Foundation

enum TypeFoodQuery {
    case productsPopular(catalogFoodId: CatalogModel)
    case productsBySearchName(foodName: String)
    case productsByStore(storeModel: StoreModel)
}
