//
//  StoresInfoViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 26.11.2021.
//

import Foundation

struct StoreModel: Equatable, Identifiable {

    // MARK: - Properties
    let id: Int?
    let name: String?
    let logo: String?

    // MARK: - Placeholders
    static let placeholder = createPlaceholder(id: 1)
    static var listPlaceholder: [StoreModel] {
        (0..<5).map({
            createPlaceholder(id: $0)
        })
    }
    private static func createPlaceholder(id: Int) -> StoreModel {
        return StoreModel(storesInfo: .init(id: id,
                                            name: "Лента",
                                            logo: ""))
    }

    // MARK: - Init
    init(storesInfo: StoresInfoResponse) {
        self.id = storesInfo.id
        self.name = storesInfo.name
        self.logo = storesInfo.logo
    }
}
