//
//  HowWeWorkModel.swift
//  Sarawan
//
//  Created by MAC on 13.11.2021.
//

import Foundation

final class HowWeWorkModel: Hashable {

    let idNumber: Int
    let icon: String
    let description: String

    init(idNumber: Int, icon: String, description: String) {
        self.idNumber = idNumber
        self.icon = icon
        self.description = description
    }

    static func == (lhs: HowWeWorkModel, rhs: HowWeWorkModel) -> Bool {
        return lhs.idNumber == rhs.idNumber
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(idNumber)
    }
}
