//
//  FiltersParameters.swift
//  SarawanSwiftUI
//
//  Created by MAC on 09.01.2022.
//

import Foundation

enum FiltersParameters {
    case upPrice
    case downPrice
    case upDiscount
    case downDiscount
    case name

    var description: String? {
        switch self {
        case .upPrice:
            return nil
        case .downPrice:
            return nil
        case .upDiscount:
            return "discount"
        case .downDiscount:
            return "-discount"
        case .name:
            return "name"
        }
    }
}
