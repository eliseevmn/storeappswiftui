//
//  CategoryViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 26.11.2021.
//

import Foundation

struct CategoryModel: Equatable, Identifiable {

    // MARK: - Properties
    let id: Int?
    let name: String?

    // MARK: - Init
    init(categoryModel: CatalogProductsListResponse.CategoryModelResponse) {
        self.id = categoryModel.id
        self.name = categoryModel.name
    }
}
