//
//  CommonProductsViewModel.swift
//  SarawanSwiftUI
//
//  Created by MAC on 25.11.2021.
//

struct ProductsCommonModel: Equatable {

    // MARK: - Properties
    let count: Int?
    let next: String?
    let previous: String?
    let products: [ProductModel]

    // MARK: - Init
    init(commonProducts: ProductsCommonInfoResponse) {
        self.count = commonProducts.count
        self.next = commonProducts.next
        self.previous = commonProducts.previous
        self.products = commonProducts.results.map { ProductModel(productModel: $0) }
    }
}
