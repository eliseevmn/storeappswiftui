//
//  TokenRepository.swift
//  SarawanSwiftUI
//
//  Created by MAC on 16.01.2022.
//

import Foundation

protocol TokenStorable: AnyObject {
    var token: String? { get set }
    var userId: Int? { get set }
}

class TokenRepository: TokenStorable {

    // MARK: - Private Properties
    // Тестовое решение, в дальнейшем будет использован Keychain
    private let keychainWrapper: UserDefaults

    // MARK: - Initialisers
    init(keychainWrapper: UserDefaults) {
        self.keychainWrapper = keychainWrapper
    }

    // MARK: - Public Properties
    var token: String? {
        get {
            keychainWrapper.string(forKey: TokenRepositoryKeys.token.rawValue)
        }
        set {
            if let newValue = newValue {
                keychainWrapper.set(newValue, forKey: TokenRepositoryKeys.token.rawValue)
            } else {
                keychainWrapper.removeObject(forKey: TokenRepositoryKeys.token.rawValue)
            }
        }
    }

    var userId: Int? {
        get {
            keychainWrapper.integer(forKey: TokenRepositoryKeys.userId.rawValue)
        }
        set {
            if let newValue = newValue {
                keychainWrapper.set(newValue, forKey: TokenRepositoryKeys.userId.rawValue)
            } else {
                keychainWrapper.removeObject(forKey: TokenRepositoryKeys.userId.rawValue)
            }
        }
    }
}

extension TokenRepository {
    private enum TokenRepositoryKeys: String {
        case token
        case userId
    }
}
