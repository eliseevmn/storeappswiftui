//
//  ContentView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 25.11.2021.
//

import SwiftUI

struct ContentView: View {

    // MARK: - Body
    var body: some View {
        MainTabBar()
    } // Body
}

// MARK: - PreviewProvider
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
