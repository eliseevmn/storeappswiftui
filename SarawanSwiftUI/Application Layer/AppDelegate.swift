//
//  AppDelegate.swift
//  SarawanSwiftUI
//
//  Created by MAC on 18.01.2022.
//

import UIKit

class AppDelegate: NSObject, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        AuthenticationViewModel(authService: nil).logout()
        return true
    }
}
