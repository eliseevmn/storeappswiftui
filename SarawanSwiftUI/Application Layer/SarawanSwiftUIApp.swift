//
//  SarawanSwiftUIApp.swift
//  SarawanSwiftUI
//
//  Created by MAC on 25.11.2021.
//

import SwiftUI
import UIKit

@main
struct SarawanSwiftUIApp: App {

    // MARK: - Properties
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    private let diContainer = DIContainer()

    // MARK: - Body
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(CategoryViewModel(productService: diContainer.productService))
                .environmentObject(AuthenticationViewModel(authService: diContainer.authService))
                .environmentObject(InternetMonitor())
                .environmentObject(ProductCardViewModel(productService: diContainer.productService))
                .environmentObject(InfoViewModel())
                .environmentObject(MainViewModel(productService: diContainer.productService,
                                                 storeService: diContainer.storesService))
                .environmentObject(HowWeWorkViewModel())
                .environmentObject(SupportProjectViewModel())
                .environmentObject(AboutProjectViewModel())
                .environmentObject(MainTabBarViewModel())
                .environmentObject(CatalogViewModel(productService: diContainer.productService))
                .environmentObject(ProfileViewModel())
        }
    } // Body
}
