//
//  AppFont.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import SwiftUI

extension Font {

//    enum MontserratWeight: String {
//        case regular = "-Regular"
//        case medium = "-Medium"
//        case semiBold = "-SemiBold"
//    }
//
//    static func montserratFont(ofSize size: CGFloat, weight: MontserratWeight = .regular) -> Font {
//        return .custom("Montserrat\(weight.rawValue)", size: size)
//    }

    enum MontserratFont {
        case regular
        case medium
        case semibold

        var value: String {
            switch self {
            case .regular:
                return "Montserrat-Regular"
            case .medium:
                return "Montserrat-Medium"
            case .semibold:
                return "Montserrat-SemiBold"
            }
        }
    }

    static func montserratFont(_ type: MontserratFont, size: CGFloat) -> Font {
        return .custom(type.value, size: size)
    }

    static func customSemiBold(size: CGFloat) -> Font {
        return .custom("Montserrat-SemiBold", size: size)
    }
    static func customRegular(size: CGFloat) -> Font {
        return .custom("Montserrat-Regular", size: size)
    }
    static func customMedium(size: CGFloat) -> Font {
        return .custom("Montserrat-Medium", size: size)
    }
}
