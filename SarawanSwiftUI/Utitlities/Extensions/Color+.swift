//
//  Color+.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import Foundation
import SwiftUI

extension Color {

    static let greenApp = Color("primary")
    static let orangeApp = Color("secondary")
    static let blackApp = Color("black")
    static let greyApp = Color("grey")
    static let whiteApp = Color("white")
    static let lightGrayApp = Color("lightGray")
    static let greenMainApp = Color("greenMain")

    static let greenBackground = Color("greenBackground")
    static let orangeBackground = Color("orangeBackground")
    static let favorableBackground = Color("favorableBackground")
}
