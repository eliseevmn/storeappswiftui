//
//  Sequence+.swift
//  SarawanSwiftUI
//
//  Created by MAC on 30.01.2022.
//

import Foundation

extension Sequence where Element: Hashable {
    func unique() -> [Element] {
        NSOrderedSet(array: self as! [Any]).array as! [Element]
    }
}
