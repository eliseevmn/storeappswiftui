//
//  String+.swift
//  SarawanSwiftUI
//
//  Created by MAC on 21.01.2022.
//

import Foundation

extension String {

    func convertSecondsTiTime(timeInSeconds: Int) -> String {
        let minutes = timeInSeconds / 60
        let seconds = timeInSeconds % 60
        return String(format: "%02i:%02i", minutes, seconds)
    }

    var digits: [Int] {
        var result = [Int]()
        for char in self {
            if let number = Int(String(char)) {
                result.append(number)
            }
        }
        return result
    }
}
