//
//  SearchTextFieldView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 04.01.2022.
//

import SwiftUI

struct SearchTextFieldView<Destination: View>: View {

    // MARK: - Properties
    @State private var searchText = ""
    @State private var isDisable = false
    var searchButtonTapped: ((String) -> Void)?
    let destination: Destination

    // MARK: - Body
    var body: some View {
        HStack {
            Image("icSearch")
                .foregroundColor(Color.greyApp)
                .padding(.leading, 10)

            TextField("Искать в Сарафане", text: $searchText)

            Image("icDictation")
                .foregroundColor(Color.greyApp)

            NavigationLink(destination: destination) {
                Text("Найти")
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(Color.greenMainApp)
                    .frame(maxHeight: .infinity)
                    .foregroundColor(Color.white)
                    .cornerRadius(10)
                    .frame(width: 120)
            } // NavigationLink
            .simultaneousGesture(TapGesture().onEnded {
                self.searchButtonTapped?(searchText)
            })
            .onDisappear(perform: {
                self.searchText = ""
            })
            .disabled(searchText.isEmpty ? !isDisable : isDisable)
        } // HStack
        .background(Color.lightGrayApp)
        .frame(height: 40)
        .cornerRadius(10)
        .overlay(RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.greenMainApp, lineWidth: 1)
                    .frame(height: 40))
        .padding(.horizontal, 20)
        .padding(.top, 0)
        .padding(.bottom, 15)
    } // Body
}
