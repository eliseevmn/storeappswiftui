//
//  Components.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProductCellView: View {

    let product: ProductModel
    let tapBuyButton: (() -> ())?
    let isShowDiscount: Bool
    @State var expend: Bool = true
    @State var countProduct: Int = 0

    var body: some View {

        ZStack {
            VStack {
                if isShowDiscount {
                    HStack {
                        Spacer()
                        Text("\(product.discountFood)%")
                            .frame(width: 50, height: 50, alignment: .center)
                            .foregroundColor(Color.whiteApp)
                            .background(Color.orangeApp)
                            .clipShape(Circle())
                    }
                    .frame(height: 1)
                    .zIndex(10)
                    .offset(y: 20)
                }

                VStack(spacing: 0) {

                    WebImage(url: URL(string: product.imageFood))
                        .resizable()
                        .placeholder(Image("icDefaultFood"))
                        .aspectRatio(contentMode: .fit)
                        .frame(height: 80)
                        .padding(.top, 20)

                    Text(product.nameFood)
                        .font(.montserratFont(.semibold, size: 16))
                        .frame(maxWidth: .infinity)
                        .frame(height: 70)
                        .padding(.top, 4)
                        .padding(.horizontal, 10)
                        .lineLimit(3)

                    HStack(spacing: 0) {
                        Text("Мин. цена: ")
                            .font(.montserratFont(.regular, size: 14))
                            .lineLimit(1)
                        Spacer()
                        Text(product.storeName)
                            .font(.montserratFont(.regular, size: 14))
                            .lineLimit(1)
                    }
                    .frame(maxWidth: .infinity)
                    .padding(.top, 18)
                    .padding(.bottom, 0)
                    .padding(.horizontal, 10)


                    HStack(spacing: 0) {
                        Text("\(product.priceFood) ₽")
                            .font(.montserratFont(.semibold, size: 18))
                            .foregroundColor(Color.orangeApp)
                        Spacer()
                        Text("\(product.unitQuantity) \(product.priceType)")
                            .font(.montserratFont(.regular, size: 14))
                    }
                    .padding(.top, 5)
                    .padding(.bottom, 10)
                    .padding(.horizontal, 10)

                    if expend {
                        Button(action: {
                            self.expend.toggle()
                            countProduct = 1
                        }) {
                            Text("Купить")
                                .font(.montserratFont(.medium, size: 18))
                                .frame(maxWidth: .infinity)
                                .padding(.vertical, 10)
                        }
                        .background(Color.green)
                        .foregroundColor(Color.white)
                        .cornerRadius(10)
                        .padding(.horizontal, 10)
                        .padding(.bottom, 10)
                    } else {
                        BuyCountButton(
                            countProduct: $countProduct,
                            increaseCountButton: {
                                countProduct += 1
                        }, reductionCountButton: {
                            countProduct -= 1
                            if countProduct < 1 {
                                self.expend.toggle()
                            }
                        })
                        .padding(.horizontal, 10)
                        .padding(.bottom, 10)
                    }

                } // VStack
                .background(Color.whiteApp)
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10.0).stroke(lineWidth: 1))
                .padding(.trailing, isShowDiscount == false ? 0 : 15)
            } // VStack
        }
    }
}

struct ProductCellView_Previews: PreviewProvider {
    static var previews: some View {
        ProductCellView(product: ProductModel.placeholder, tapBuyButton: nil, isShowDiscount: true)
    }
}
