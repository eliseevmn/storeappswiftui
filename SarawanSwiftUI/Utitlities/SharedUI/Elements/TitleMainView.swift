//
//  TitleMainView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 03.01.2022.
//

import SwiftUI

struct TitleMainView: View {

    // MARK: - Properties
    let titleName: String

    // MARK: - Body
    var body: some View {
        HStack {
            Text(titleName)
                .frame(alignment: .leading)
                .font(.montserratFont(.medium, size: 24))
            Spacer()
        } // HStack
    } // Body
}

// MARK: - PreviewProvider
struct TitleMainView_Previews: PreviewProvider {
    static var previews: some View {
        TitleMainView(titleName: "Выгодные предложения")
    }
}
