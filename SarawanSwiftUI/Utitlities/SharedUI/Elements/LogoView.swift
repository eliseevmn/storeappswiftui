//
//  LogoView.swift
//  SarawanSwiftUI
//
//  Created by MAC on 04.01.2022.
//

import SwiftUI

struct LogoView: View {
    
    var body: some View {
        HStack(alignment: .bottom) {
            Image("icLogo")
                .resizable()
                .frame(width: 37, height: 22)

            Image("icLogoTitle")
                .resizable()
                .frame(width: 101, height: 25)
                .offset(y: 3)
                .foregroundColor(Color.greenMainApp)

            Spacer()
        }
        .padding()
    }
}

struct LogoView_Previews: PreviewProvider {
    static var previews: some View {
        LogoView()
    }
}
