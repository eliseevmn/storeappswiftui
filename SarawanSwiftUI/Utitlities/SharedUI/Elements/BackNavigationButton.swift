//
//  BackNavigationButton.swift
//  SarawanSwiftUI
//
//  Created by MAC on 07.01.2022.
//

import SwiftUI

struct BackNavigationButton: View {

    // MARK: - Properties
    let title: String
    let dismissButton: (() -> Void)

    // MARK: - Body
    var body: some View {
        HStack {
            Button(action: dismissButton) {
                Image("icArrowBack")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 30, height: 35, alignment: .leading)
                    .foregroundColor(Color.blackApp)
            }
            Text(title)
                .font(.montserratFont(.semibold, size: 24))
            Spacer()
        } // HStack
        .padding()
    } // Body
}

// MARK: - PreviewProvider
struct BackNavigationButton_Previews: PreviewProvider {
    static var previews: some View {
        BackNavigationButton(title: "Как мы работаем", dismissButton: {

        })
    }
}
