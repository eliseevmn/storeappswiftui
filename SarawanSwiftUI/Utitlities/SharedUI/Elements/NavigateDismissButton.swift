//
//  ProducCardDIsmissButton.swift
//  SarawanSwiftUI
//
//  Created by MAC on 12.01.2022.
//

import SwiftUI

struct NavigateDismissButton: View {

    // MARK: - Properties
    var onTapDissmissButton: (() -> Void)

    // MARK: - Body
    var body: some View {
        HStack {
            Spacer()
            Button(action: onTapDissmissButton) {
                Image("icClose")
                    .resizable()
                    .frame(width: 40, height: 40)
                    .aspectRatio(contentMode: .fit)
                    .foregroundColor(Color.blackApp)
            } // Button
        } // HStack
    } // Body
}

// MARK: - PreviewProvider
struct ProducCardDismissButton_Previews: PreviewProvider {
    static var previews: some View {
        NavigateDismissButton(onTapDissmissButton: {
            
        })
    }
}
