//
//  BuyCountButton.swift
//  SarawanSwiftUI
//
//  Created by MAC on 30.01.2022.
//

import SwiftUI

struct BuyCountButton: View {

    // MARK: - Properties
    @Binding var countProduct: Int
    var increaseCountButton: (() -> Void)
    var reductionCountButton: (() -> Void)

    // MARK: - Body
    var body: some View {

        ZStack {

            Color.red
                .frame(maxWidth: .infinity)
                .frame(maxHeight: .infinity)

//            HStack {
//                Button(action: reductionCountButton) {
//                    Image(systemName: "minus")
//                        .foregroundColor(Color.greenApp)
//                        .font(.montserratFont(.medium, size: 18))
//                }
//
//                Spacer()
//
//                Text("\(countProduct)")
//                    .foregroundColor(Color.greenApp)
//                    .font(.montserratFont(.medium, size: 18))
//                    .padding(.horizontal)
//                    .padding(.vertical, 10)
//
//                Spacer()
//
//                Button(action: increaseCountButton) {
//                    Image(systemName: "plus")
//                        .foregroundColor(Color.greenApp)
//                        .font(.montserratFont(.medium, size: 18))
//                }
//            } // HStack
//            .padding(.horizontal, 9)
//            .frame(maxWidth: .infinity)
//            .background(Color.whiteApp)
//            .overlay(
//                RoundedRectangle(cornerRadius: 10)
//                    .stroke(lineWidth: 1)
//                    .foregroundColor(Color.greenApp)
//            )
        }
    } // Body
}

// MARK: - PreviewProvider
struct BuyCountButton_Previews: PreviewProvider {
    static var previews: some View {
        BuyCountButton(countProduct: .constant(1),
                       increaseCountButton: {},
                       reductionCountButton: {})
    }
}

struct NoHitTesting: ViewModifier {
  func body(content: Content) -> some View {
    SwiftUIWrapper { content }.allowsHitTesting(false)
  }
}
extension View {
  func userInteractionDisabled() -> some View {
    self.modifier(NoHitTesting())
  }
}
struct SwiftUIWrapper<T: View>: UIViewControllerRepresentable {
  let content: () -> T
  func makeUIViewController(context: Context) -> UIHostingController<T> {
    UIHostingController(rootView: content())
  }
  func updateUIViewController(_ uiViewController: UIHostingController<T>, context: Context) {}
}
