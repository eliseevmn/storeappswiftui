//
//  TitleTextStyle.swift
//  SarawanSwiftUI
//
//  Created by MAC on 12.01.2022.
//

import SwiftUI

struct TitleTextModifier: ViewModifier {

    var font: Font

    func body(content: Content) -> some View {
        content
            .font(font)
            .foregroundColor(Color.blackApp)
            .frame(maxWidth: .infinity, alignment: .leading)
    }
}
